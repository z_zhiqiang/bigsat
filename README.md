### Repo Structure
* **/inputs**: contain all the input text files.
* **/src**: contain the java source code.
* **/targert**: contain compiled classes, builds, jars, etc
* **pom.xml**: maven config file

### How To Run
1. Compile and build the source code using maven with command: `mvn clean install`
2. Run the file on the master server (default: zion-1) with the command: `spark-submit <jar_file> <input_file>`. Example: `spark-submit target/prototype-1.0.jar inputs/myTest.txt`