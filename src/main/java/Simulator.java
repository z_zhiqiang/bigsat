import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

public class Simulator {
	
	public static void main(String[] args) {
		int number_buckets = 10;
		int number_literals = 4;
		int number_clauses = 1000000;
		
		Random ran = new Random();
		Map<Integer, List<int[]>> collections = new HashMap<Integer, List<int[]>>();
		
		for(int i = 0; i < number_clauses; i++){
			Set<Integer> set = new HashSet<Integer>();
			int key = Integer.MAX_VALUE;

			int[] clause = new int[number_literals];
			for(int j = 0; j < number_literals;){
				int literal = ran.nextInt(number_buckets) + 1;
				if(set.add(literal)){
					clause[j++] = literal;
					if(key > literal){
						key = literal;
					}
				}
			}
//			printArray(clause);
			
			//add a clause into the respective bucket
			if(collections.containsKey(key)){
				collections.get(key).add(clause);
			}
			else{
				List<int[]> list = new ArrayList<int[]>();
				list.add(clause);
				collections.put(key, list);
			}
		}
		
		
		//print out the statistics data
		NumberFormat formatter = new DecimalFormat("#0.0000");   
		for(int i = 1; i <= number_buckets; i++){
			//the probability for a clause belonging to bucket i is: 
			// Combination(number_bucket - i, number_literals - 1) / Combination(number_bucket, number_literals)
			double p = combination_rec(number_buckets - i, number_literals - 1) / combination_rec(number_buckets, number_literals);
			if(collections.containsKey(i)){
				System.out.println(i + ":\t" + collections.get(i).size() + ":\t\t" + formatter.format(p));
			}
			else{
				System.out.println(i + ":\t0:\t\t" + formatter.format(p));
			}
		}
		
	}
	
	public static void printArray(int[] array){
		System.out.print("[");
		for(int i = 0; i < array.length; i++){
			System.out.print(array[i]);
			if(i != array.length - 1){
				System.out.print(", ");
			}
		}
		System.out.println("]");
	}
	
	
	public static double combination_rec(int n, int r){
		if(r > n){
			return 0;
		}
		if(r == 0 || r == n){
			return 1;
		}
		return combination_rec(n - 1, r - 1) + combination_rec(n - 1, r);
	}

}
