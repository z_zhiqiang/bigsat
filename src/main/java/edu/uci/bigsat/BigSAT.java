package edu.uci.bigsat;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import edu.uci.bigsat.datastructure.MyZBDDClauseCollection;
import edu.uci.bigsat.preprocessor.Preprocessor;
import edu.uci.bigsat.solver.AbstractSolver;
import edu.uci.bigsat.solver.DPCollectionSolver;
import edu.uci.bigsat.solver.DPCollectionSolver_Collection;
import edu.uci.bigsat.solver.DPCollectionSolver_Collection_Pair;
import edu.uci.bigsat.solver.DResSolver;

public class BigSAT {
	//for debugging
	public static int tcount = 0;
	public static long splittime = 0;
	public static long resolutiontime = 0;
	public static long serializetime = 0;
	
	public static void main(String[] args) {
		if(args.length != 4){
			System.err.println("Usage: input_file number_buckets number_partitions number_splits");
			System.exit(0);
		}
		
		long stime = System.currentTimeMillis();
		
		//set logger
		Logger.getLogger("org").setLevel(Level.OFF);
		Logger.getLogger("akka").setLevel(Level.OFF);
		
		//get parameters
		final int reordering = 1;
		final int mode = 3;
		final int ds_mode = 3;
		final String input = args[0];
		final int number_buckets = Integer.parseInt(args[1]);
		final int number_partitions = Integer.parseInt(args[2]);
		final int number_splits = Integer.parseInt(args[3]);
		
		//initiate Spark context
		JavaSparkContext jsc = initiateSparkContext();
		
		//preprocess to remove duplicated literals and tautological clauses, and to reorder variables for resolution via variable renaming
		JavaRDD<int[]> renamedCNF = Preprocessor.preprocess(reordering, jsc, input, number_buckets, number_partitions);
		long petime = System.currentTimeMillis();
		
		//perform resolution
		getSolver(mode).doResolution(ds_mode, jsc, number_buckets, renamedCNF, number_partitions, number_splits);
		
		//print out time cost
		long etime = System.currentTimeMillis();
		System.out.println("\n\nPreprocess time cost:\t" + (petime - stime) / 1000);
		System.out.println("\nComputation time cost:\t" + (etime - petime) / 1000);
		System.out.println("\nTotal time: \t" + (etime - stime) / 1000);
		System.out.println("\nTotal counts of resolution:\t" + tcount);
		System.out.println("\nSplit time:\t" + splittime / 1000);
		System.out.println("\nResolution time:\t" + resolutiontime / 1000);
		System.out.println("\nSerialization time:\t" + serializetime / 1000);
	}


	/**This is to initialize the Spark context
	 * @return
	 */
	private static JavaSparkContext initiateSparkContext(){
		SparkConf conf = new SparkConf().setAppName("SAT")
      		.setMaster("local[4]")
      
//				.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
//				.set("spark.kryoserializer.buffer.mb","24") 
//				.set("spark.cores.max", "1");
				;
		return new JavaSparkContext(conf);
	}
	
	
	/**This is a solver factory, which returns the respective singleton solver instance according to the mode. 
	 * @param mode
	 * @return
	 */
	private static AbstractSolver getSolver(int mode){
		AbstractSolver solver = null;
		
		switch (mode) {
//		case 1:
//			solver = DPCollectionSolver.getInstance();
//			break;
//		case 2: 
//			solver = DPCollectionSolver_Collection.getInstance();
//			break;
		case 3:
			solver = DPCollectionSolver_Collection_Pair.getInstance();
			break;
//		case 4:
//			solver = DResSolver.getInstance();
//			break;
		default:
			throw new RuntimeException("wrong type!!!");
		}
		
		return solver;
	}
	

}
