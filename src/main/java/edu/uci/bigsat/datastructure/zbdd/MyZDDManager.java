package edu.uci.bigsat.datastructure.zbdd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import scala.Tuple2;


public class MyZDDManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8961290854293724905L;
	
	public static final MyZDDNode ZERO = new MyZDDNode(0, null, null);
	public static final MyZDDNode ONE = new MyZDDNode(-1, null, null);
	
	private Map<MyZDDNode, MyZDDNode> unique_map;
	
	
	public MyZDDManager(){
		this.unique_map = new HashMap<MyZDDNode, MyZDDNode>();
	}
	
	
	public void cleanup(){
		this.unique_map.clear();
		this.unique_map = null;
	}
	
//	private void writeObject(ObjectOutputStream oos) throws IOException {
//		// default serialization
//		this.map = null;
//		oos.defaultWriteObject();
//		// write the object
////		System.exit(0);
//	}
//
//	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
//		// default deserialization
//		ois.defaultReadObject();
//		this.map = null;
////		System.exit(0);
//	}
	

	
//	//---------------------------------------------- anti-resolution ---------------------------------------------
//	
//	public List<MyZDDNode> anti_resolution(MyZDDNode root){
//		Map<MyZDDNode, MyZDDNode> cut_nodes = find(root);
//		cut(cut_nodes, root);
//		List<MyZDDNode> list = append(cut_nodes);
//		list.add(root);
//		return list;
//	}
//	
//	
//	public Map<MyZDDNode, MyZDDNode> find(MyZDDNode root){
//		Map<MyZDDNode, MyZDDNode> cut_nodes = new HashMap<MyZDDNode, MyZDDNode>();
//		
//		int range_unit = 10;
//		find_recusive_range(range_unit, range_unit, root, cut_nodes);
//		
//		return cut_nodes;
//	}
//	
//	private void find_recusive_range(final int range_unit, int range, MyZDDNode root, Map<MyZDDNode, MyZDDNode> cut_nodes) {
//		// TODO Auto-generated method stub
//		if(root.isONE() || root.isZERO()){
//			return;
//		}
//		
//		if(root.getValue() > range){
//			cut_nodes.put(root, null);
//			find_recusive_range(range_unit, range + range_unit, root.getHigh(), cut_nodes);
//			find_recusive_range(range_unit, range + range_unit, root.getLow(), cut_nodes);
//		}
//		else{
//			find_recusive_range(range_unit, range, root.getHigh(), cut_nodes);
//			find_recusive_range(range_unit, range, root.getLow(), cut_nodes);
//		}
//		
//	}
//
//
//	public void cut(Map<MyZDDNode, MyZDDNode> cut_nodes, MyZDDNode root){
//		if(cut_nodes.isEmpty()){
//			return;
//		}
//		if(root.isONE() || root.isZERO()){
//			return;
//		}
//		
//		MyZDDNode low = root.getLow();
//		MyZDDNode high = root.getHigh();
//		
//		//high
//		if(cut_nodes.containsKey(high)){
//			MyZDDNode newNode = null;
//			
//			if(cut_nodes.get(high) != null){
//				newNode = cut_nodes.get(high);
//			}
//			else{
//				int newValue = getUniqueValue();
//				newNode = this.mk(newValue, ZERO, ONE);
//				cut_nodes.put(high, newNode);
//			}
//			
//			this.map.remove(new Triple(root.getValue(), root.getLow().hashCode(), root.getHigh().hashCode()));
//			root.setHigh(newNode);
//			this.map.put(new Triple(root.getValue(), root.getLow().hashCode(), root.getHigh().hashCode()), root);
//		}
//
//		cut(cut_nodes, high);
//		
//		//low
//		if(cut_nodes.containsKey(low)){
//			MyZDDNode newNode = null;
//			
//			if(cut_nodes.get(low) != null){
//				newNode = cut_nodes.get(low);
//			}
//			else{
//				int newValue = getUniqueValue();
//				newNode = this.mk(newValue, ZERO, ONE);
//				cut_nodes.put(low, newNode);
//			}
//			
//			this.map.remove(new Triple(root.getValue(), root.getLow().hashCode(), root.getHigh().hashCode()));
//			root.setLow(newNode);
//			this.map.put(new Triple(root.getValue(), root.getLow().hashCode(), root.getHigh().hashCode()), root);
//		}
//		
//		cut(cut_nodes, low);
//	}
//	
//	public static int getUniqueValue() {
//		// TODO Auto-generated method stub
//		Random random = new Random();
////		try {
////			Thread.sleep(3);
////		} catch (InterruptedException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
//		return (int) (System.currentTimeMillis() % 100) + 200 
//				+ random.nextInt(100)
//				;
//	}
//
//	
//	public List<MyZDDNode> append(Map<MyZDDNode, MyZDDNode> map){
//		List<MyZDDNode> list = new ArrayList<MyZDDNode>();
//		if(map.isEmpty()){
//			return list;
//		}
//		
//		for(MyZDDNode node: map.keySet()){
//			int value = map.get(node).getValue();
//			MyZDDNode anotherNode = this.mk(value % 2 == 0 ? value - 1 : value + 1, ZERO, ONE);
//			list.add(this.productSTFree(node, anotherNode));
//		}
//		return list;
//	}
//	
//	//---------------------------------------------- anti-resolution ---------------------------------------------

	
//	/** rebuild a zdd under the current manager 
//	 * @param zdd
//	 * @return
//	 */
//	public final MyZDDNode rebuildZBDD(MyZDDNode zdd){
//		if(zdd.isONE()){
//			return ONE;
//		}
//		if(zdd.isZERO()){
//			return ZERO;
//		}
//		
//		MyZDDNode low = rebuildZBDD(zdd.getLow());
//		MyZDDNode high = rebuildZBDD(zdd.getHigh());
//		return mk(zdd.getValue(), low, high);
//	}
	
	/** The basic operation to make a node in ZBDDs
	 * @param val
	 * @param ln
	 * @param hn
	 * @return
	 */
	public final MyZDDNode mk(int val, MyZDDNode ln, MyZDDNode hn){
		//zero node elimination
		if(hn.isZERO()){
			return ln;
		}
		
		//tautology elimination
		if((val + 1) % 2 == 0 && hn.getValue() == val + 1){
			return ln;
		}
		
		MyZDDNode newNode = new MyZDDNode(val, ln, hn);
		if(this.unique_map.containsKey(newNode)){
			return this.unique_map.get(newNode);
		}
		else{
			this.unique_map.put(newNode, newNode);
			return newNode;
		}
	}
	
	public final static MyZDDNode mk_path(int val, MyZDDNode ln, MyZDDNode hn){
		//zero node elimination
		if(hn.isZERO()){
			return ln;
		}
		
		//tautology elimination
		if((val + 1) % 2 == 0 && hn.getValue() == val + 1){
			return ln;
		}
		
		MyZDDNode newNode = new MyZDDNode(val, ln, hn);
		return newNode;
	}
	
	/** create a zdd representing a clause
	 * @param list
	 * @return
	 */
	public final MyZDDNode createPath(int[] list){
		MyZDDNode last = ONE;
		for(int index = list.length - 1; index >= 0; index--){
			int i = list[index];
			int j = i > 0 ? 2 * i : 2 * Math.abs(i) - 1;
			last = mk(j, ZERO, last);
		}
		return last;
	}
	
	public final MyZDDNode createPath(List<Integer> list){
		MyZDDNode last = ONE;
		for(int index = list.size() - 1; index >= 0; index--){
			int i = list.get(index);
			int j = i > 0 ? 2 * i : 2 * Math.abs(i) - 1;
			last = mk(j, ZERO, last);
		}
		return last;
	}
	
	public final static MyZDDNode createPath_static(List<Integer> list){
		MyZDDNode last = ONE;
		for(int index = list.size() - 1; index >= 0; index--){
			int i = list.get(index);
			int j = i > 0 ? 2 * i : 2 * Math.abs(i) - 1;
			last = mk_path(j, ZERO, last);
		}
		return last;
	}
	
//	/**regular intersection of two ZBDDs
//	 * @param p
//	 * @param q
//	 * @return
//	 */
//	public MyZDDNode intersect(MyZDDNode p, MyZDDNode q){
//		if(p.isZERO() || q.isZERO()){
//			return ZERO;
//		}
//		if(p == q){
//			return p;
//		}
//		if(p.isONE()){
//			return follow_low(q);
//		}
//		if(q.isONE()){
//			return follow_low(p);
//		}
//		
//		MyZDDNode retNode;
//		if(p.getValue() < q.getValue()){
//			retNode = intersect(p.getLow(), q);
//		}
//		else if(p.getValue() > q.getValue()){
//			retNode = intersect(p, q.getLow());
//		}
//		else{
//			retNode = intersect(p.getLow(), q.getLow());
//			MyZDDNode hn = intersect(p.getHigh(), q.getHigh());
//			retNode = mk(p.getValue(), retNode, hn);
//		}
//		return retNode;
//	}
	
	private final MyZDDNode follow_low(MyZDDNode zdd) {
		// TODO Auto-generated method stub
		while (!zdd.isZERO() && !zdd.isONE()){
			zdd = zdd.getLow();
		}
		return zdd;
	}
	
	private final MyZDDNode follow_high(MyZDDNode zdd) {
		// TODO Auto-generated method stub
		while (!zdd.isZERO() && !zdd.isONE()){
			zdd = zdd.getHigh();
		}
		return zdd;
	}

	private final boolean emptyIn(MyZDDNode znode){
		return (follow_low(znode).isONE());
	}

	
//	/**regular union of two ZBDDs
//	 * @param p
//	 * @param q
//	 * @return
//	 */
//	public MyZDDNode union(MyZDDNode p, MyZDDNode q){
//		if(p.isZERO()){
//			return q;
//		}
//		
//		if(q.isZERO() || p == q){
//			return p;
//		}
//		
//		if(p.isONE()){
//			return insert_base(q);
//		}
//		if(q.isONE()){
//			return insert_base(p);
//		}
//		
//		if(p.getValue() < q.getValue()){
//			return union(q, p);
//		}
//
//		MyZDDNode retNode;
//		if(p.getValue() > q.getValue()){
//			retNode = union(p, q.getLow());
//			retNode = mk(q.getValue(), retNode, q.getHigh());
//		}
//		else{
//			retNode = union(p.getLow(), q.getLow());
//			MyZDDNode h = union(p.getHigh(), q.getHigh());
//			retNode = mk(p.getValue(), retNode, h);
//		}
//		
//		return retNode;
//		
//	}
	
//	private MyZDDNode insert_base(MyZDDNode zdd) {
//		// TODO Auto-generated method stub
//		if(zdd.isONE() || zdd.isZERO()){
//			return ONE;
//		}
//		MyZDDNode node = insert_base(zdd.getLow());
//		node = (zdd.getLow() == node) ? zdd : mk(zdd.getValue(), node, zdd.getHigh());
//		
//		return node;
//	}


	/**subsumed difference, i.e., Definition 2 in Laurent Simon's paper
	 * @param p
	 * @param q
	 * @return
	 */
	public MyZDDNode subsumedDiff(MyZDDNode p, MyZDDNode q){
		if(emptyIn(q))
			return ZERO;
		return subsumedDiff_rec(p, q);
	}
	
	private MyZDDNode subsumedDiff_rec(MyZDDNode f, MyZDDNode c) {
		// TODO Auto-generated method stub
		if(f.isZERO() || c.isONE() || f == c)
			return ZERO;
		if(f.isONE() || c.isZERO())
			return f;
		
		MyZDDNode retNode;
		int fvar = f.getValue();
		int cvar = c.getValue();
		
		if(fvar > cvar){
			retNode = subsumedDiff_rec(f, c.getLow());
		}
		else if(fvar < cvar){
			retNode = mk(fvar, subsumedDiff_rec(f.getLow(), c), subsumedDiff_rec(f.getHigh(), c));
		}
		else{
			MyZDDNode tmp1, tmp2;
			if(emptyIn(c.getHigh())){
				tmp1 = ZERO;
			}
			else{
//				tmp1 = subsumedDiff_rec(f.getHigh(), c.getLow());
//				tmp2 = subsumedDiff_rec(f.getHigh(), c.getHigh());
//				tmp1 = intersect(tmp1, tmp2);
				tmp1 = subsumedDiff_rec(subsumedDiff_rec(f.getHigh(), c.getHigh()), c.getLow());
			}
			
			tmp2 = subsumedDiff_rec(f.getLow(), c.getLow());
			retNode = mk(fvar, tmp2, tmp1);
		}
		
		return retNode;
	}
	
	
	/** Subsumption elimination, Definition 3 in Laurent Simon's paper
	 * @param zdd
	 * @return
	 */
	public final MyZDDNode noSubsumption(MyZDDNode zdd){
		if(zdd.isONE())
			return ONE;
		if(zdd.isZERO())
			return ZERO;
		
		MyZDDNode low = zdd.getLow();
		MyZDDNode high = zdd.getHigh();
		MyZDDNode ret;
		if(low == high){
			ret = noSubsumption(low);
		}
		else{
			MyZDDNode tmpl = noSubsumption(low);
			MyZDDNode tmph = noSubsumption(high);
			tmph = subsumedDiff(tmph, tmpl);
			ret = mk(zdd.getValue(), tmpl, tmph);
		}
		
		return ret;
	}
	
	
	/** Subsumption-free union, Definition 4 in Laurent Simon's paper
	 * @param p
	 * @param q
	 * @return
	 */
	public final MyZDDNode unionSubsumFree(MyZDDNode p, MyZDDNode q){
		if(p.isZERO()){
			return noSubsumption(q); 
		}
		if(q.isZERO() || p == q){
			return noSubsumption(p);
		}
		if(p.isONE() || q.isONE()){
			return ONE;
		}

		if(p.getValue() < q.getValue()){
			return unionSubsumFree(q, p);
		}
		
		MyZDDNode tmpl, tmph;
		MyZDDNode ret;
		if(p.getValue() > q.getValue()){
			tmpl = unionSubsumFree(q.getLow(), p);
			tmph = subsumedDiff(noSubsumption(q.getHigh()), tmpl);
			ret = mk(q.getValue(), tmpl, tmph);
		}
		else{
			tmpl = unionSubsumFree(p.getLow(), q.getLow());
			tmph = unionSubsumFree(p.getHigh(), q.getHigh());
			tmph = subsumedDiff(tmph, tmpl);
			ret = mk(p.getValue(), tmpl, tmph);
		}

		return ret;
	}
	
	/**product of two ZBDDs while removing all the subsumed and tautological clauses 
	 * @param p
	 * @param q
	 * @return
	 */
	public final MyZDDNode productSTFree(MyZDDNode p, MyZDDNode q){
		if(p.isZERO() || q.isZERO())
			return ZERO;
		if(p.isONE())
			return noSubsumption(q);
		if(q.isONE())
			return noSubsumption(p);
		
		if(p.getValue() > q.getValue()) return productSTFree(q,p);
		
		MyZDDNode ret;
		if(isNegative(p)){
			if(isConsecutive(p)){//R1, R2 and R3 of Definition 5 in Laurent Simon's paper
				MyZDDNode a1 = p.getHigh();
				MyZDDNode a2 = p.getLow().getHigh();
				MyZDDNode a3 = p.getLow().getLow();
				
				if(hasEqualVar(p, q)){//R1 and R2 
					if(isConsecutive(q)){//R1: (-x, (x, A3, A2), A1) * (-x, (x, B3, B2), B1)
						MyZDDNode b1 = q.getHigh();
						MyZDDNode b2 = q.getLow().getHigh();
						MyZDDNode b3 = q.getLow().getLow();
						
						MyZDDNode tmpa3b3 = productSTFree(a3, b3);
						MyZDDNode tmpa1b1 = productSTFree(a1, b1);
						MyZDDNode tmpa1b3 = productSTFree(a1, b3);
						MyZDDNode tmpb1a3 = productSTFree(b1, a3);
						MyZDDNode tmpr1 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b3), tmpb1a3), tmpa3b3);
						
						MyZDDNode tmpa2b2 = productSTFree(a2, b2);
						MyZDDNode tmpa2b3 = productSTFree(a2, b3);
						MyZDDNode tmpb2a3 = productSTFree(b2, a3);
						MyZDDNode tmpr2 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa2b2, tmpa2b3), tmpb2a3), tmpa3b3);
						
						ret = mk(p.getValue(), mk(p.getLow().getValue(), tmpa3b3, tmpr2), tmpr1);
						
						return ret;
					}
					else{//R2: (-x, (x, A3, A2), A1) * (-x, B2, B1) s.t. getVar(B2) != x
						MyZDDNode b1 = q.getHigh();
						MyZDDNode b2 = q.getLow();
						
						MyZDDNode tmpa3b2 = productSTFree(a3, b2);
						MyZDDNode tmpa1b1 = productSTFree(a1, b1);
						MyZDDNode tmpa1b2 = productSTFree(a1, b2);
						MyZDDNode tmpb1a3 = productSTFree(b1, a3);
						MyZDDNode tmpr1 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b2), tmpb1a3), tmpa3b2);
						
						MyZDDNode tmpa2b2 = productSTFree(a2, b2);
						MyZDDNode tmpr2 = subsumedDiff(tmpa2b2, tmpa3b2);
						
						ret = mk(p.getValue(), mk(p.getLow().getValue(), tmpa3b2, tmpr2), tmpr1);
						
						return ret;
					}
				}
				else if(hasConsecutiveVar(p, q)){//R3: (-x, (x, A3, A2), A1) * (x, B2, B1)
					MyZDDNode b1 = q.getHigh();
					MyZDDNode b2 = q.getLow();
					
					MyZDDNode tmpa3b2 = productSTFree(a3, b2);
					MyZDDNode tmpa1b2 = productSTFree(a1, b2);
					MyZDDNode tmpr1 = subsumedDiff(tmpa1b2, tmpa3b2);
					
					MyZDDNode tmpa2b1 = productSTFree(a2, b1);
					MyZDDNode tmpa2b2 = productSTFree(a2, b2);
					MyZDDNode tmpb1a3 = productSTFree(b1, a3);
					MyZDDNode tmpr2 = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa2b1, tmpa2b2), tmpb1a3), tmpa3b2);
					
					ret = mk(p.getValue(), mk(p.getLow().getValue(), tmpa3b2, tmpr2), tmpr1);
					
					return ret;
				}
				
			}
			else if(hasEqualVar(p, q) && isConsecutive(q)){//symmetry of R2
				return productSTFree(q,p);
			}
			else if(hasConsecutiveVar(p, q)){//R4: (-x, A2, A1) * (x, B2, B1) s.t. getVar(A2) != x
				MyZDDNode a1 = p.getHigh();
				MyZDDNode a2 = p.getLow();
				MyZDDNode b1 = q.getHigh();
				MyZDDNode b2 = q.getLow();
				
				MyZDDNode tmpa2b2 = productSTFree(a2, b2);
				MyZDDNode tmpa1b2 = productSTFree(a1, b2);
				MyZDDNode tmpr1 = subsumedDiff(tmpa1b2, tmpa2b2);
				
				MyZDDNode tmpa2b1 = productSTFree(a2, b1);
				MyZDDNode tmpr2 = subsumedDiff(tmpa2b1, tmpa2b2);
				
				ret = mk(p.getValue(), mk(q.getValue(), tmpa2b2, tmpr2), tmpr1);

				return ret;
			}
		}
		
		
		if(hasEqualVar(q, p)){//R5
			MyZDDNode a1 = p.getHigh();
			MyZDDNode a2 = p.getLow();
			MyZDDNode b1 = q.getHigh();
			MyZDDNode b2 = q.getLow();
			
			MyZDDNode tmpa2b2 = productSTFree(a2, b2);
			MyZDDNode tmpa1b1 = productSTFree(a1, b1);
			MyZDDNode tmpa2b1 = productSTFree(a2, b1);
			MyZDDNode tmpa1b2 = productSTFree(a1, b2);
			MyZDDNode tmprh = subsumedDiff(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa2b1), tmpa1b2), tmpa2b2);
			
			ret = mk(p.getValue(), tmpa2b2, tmprh);
			
			return ret;
		}
		else{//R6 and R7 which are symmetry
			MyZDDNode tmpa2q = productSTFree(p.getLow(), q);
			MyZDDNode tmpa1q = productSTFree(p.getHigh(), q);
			MyZDDNode tmprh = subsumedDiff(tmpa1q, tmpa2q);
			
			ret = mk(p.getValue(), tmpa2q, tmprh);
			
			return ret;
		}
		
		
	}
	
	/**p and q have consecutive label values, i.e., p = (-x, plow_, phigh_), q = (x, qlow_, qhigh_)
	 * @param p
	 * @param q
	 * @return
	 */
	private boolean hasConsecutiveVar(MyZDDNode p, MyZDDNode q) {
		return q.getValue() == p.getValue() + 1;
	}


	/**p and q have same label values, i.e., p = (l, plow_, phigh_), q = (l, qlow_, qhigh_)
	 * @param p
	 * @param q
	 * @return
	 */
	private boolean hasEqualVar(MyZDDNode p, MyZDDNode q) {
		return q.getValue() == p.getValue();
	}


	/**p's low has consecutive values with p, i.e., p = (-x, (x, plow_low_, plow_high_), phigh_)
	 * @param p
	 * @return
	 */
	public static boolean isConsecutive(MyZDDNode p) {
		return p.getLow().getValue() == p.getValue() + 1;
	}


	/**p's value is negative, i.e., p = (-x, plow_, phigh_)
	 * @param p
	 * @return
	 */
	private static boolean isNegative(MyZDDNode p) {
		return (p.getValue() + 1) % 2 == 0;
	}
	
	public static boolean isNegConsecutive(MyZDDNode p){
		return isNegative(p) && isConsecutive(p);
	}

	/** returns {} */
	public final MyZDDNode empty() {
		return ZERO;
	}

	/** returns {{}} */
	public final MyZDDNode base() {
		return ONE;
	}


//	public void markNode(MyZDDNode zdd) {
//		// TODO Auto-generated method stub
//		this.marked_set.add(zdd);
//	}
//
//
//	public boolean isMarked(MyZDDNode zdd) {
//		// TODO Auto-generated method stub
//		return this.marked_set.contains(zdd);
//	}
//
//
//	public void clearMarkedSet() {
//		// TODO Auto-generated method stub
//		this.marked_set.clear();
//	}
	
	
	public MyZDDNode getSubset(MyZDDNode zdd, int value){
		if(zdd.isONE() || zdd.isZERO()){
			return ZERO;
		}
		
		if(zdd.getValue() > value){
			return ZERO;
		}
		if(zdd.getValue() == value){
			return zdd.getHigh();
		}
		MyZDDNode low = getSubset(zdd.getLow(), value);
		MyZDDNode high = getSubset(zdd.getHigh(), value);
		MyZDDNode node = mk(zdd.getValue(), low, high);
		
		return node;
	}
	
	
	/**split zdd into two disjoint zdds, such that the first generated zdd with cut_count size
	 * @param zdd
	 * @param cut_count
	 * @return
	 */
	public Tuple2<MyZDDNode, MyZDDNode> splitZDD(MyZDDNode zdd, long cut_count){
		assert(cut_count <= zdd.count());
//		assert(cut_count > 2);
		
		if(zdd.count() <= 2){
			return new Tuple2<MyZDDNode, MyZDDNode>(ZERO, zdd);
		}
		
		MyZDDNode cut_zdd = extractSubset_rec(zdd, cut_count, null, ONE, ZERO);
		MyZDDNode remain_zdd = this.subsumedDiff(zdd, cut_zdd);
		return new Tuple2<MyZDDNode, MyZDDNode>(cut_zdd, remain_zdd);
	}

	private MyZDDNode extractSubset_rec(MyZDDNode zdd, long count, MyZDDNode parent, MyZDDNode prefix, MyZDDNode result){
		if(count < zdd.count()){
			return extractSubset_rec(zdd.getLow(), count, zdd, prefix, result);
		}
		else if(count > zdd.count()){
			count -= zdd.count();
			result = this.unionSubsumFree(result, this.productSTFree(zdd, prefix));
			return extractSubset_rec(parent.getHigh(), count, parent, this.productSTFree(prefix, this.mk(parent.getValue(), ZERO, ONE)), result);
		}
		else{
			return this.unionSubsumFree(result, this.productSTFree(zdd, prefix));
		}
	}
	
	
	public static void main(String[] args) {
		MyZDDManager zddManager = new MyZDDManager();
		
//		int[] a1 = {1, -2, 4, 5, 7, 8, 9, -10, 13, -14, 15};
//		int[] a2 = {-1, 2, 3, 5, 7, 8, 9, 11, -14, 15};
//		int[] a3 = {1, 3};
//		int[] a4 = {4, -5, -9};
//		
//		MyZDDNode z1 = zddManager.createPath(a1);
//		MyZDDNode z2 = zddManager.createPath(a2);
//		MyZDDNode z3 = zddManager.createPath(a3);
//		MyZDDNode z4 = zddManager.createPath(a4);
		
		int[][] arrays = {
				{4, 5, 7, 8, 9, -10, 13, -14, 15},
				{3, 5, 7, 8, 9, 11, -14, 15},
				{1, 2, 3},
				{4, -5, -9},
				{5, -6, 9},
				{1, -2, 3},
				{6, 9, 11, -14, 15}
				};
		
		MyZDDNode zdd = zddManager.ZERO;
		for(int[] array: arrays){
			MyZDDNode z = zddManager.createPath(array);
			zdd = zddManager.unionSubsumFree(zdd, z);
		}
		
		System.out.println(zdd.count());
		System.out.println(MyZDDPrinter.getPaths(zdd, zddManager));
		System.out.println();
		
//		MyZDDNode cutZdd = zddManager.extractSubset(zdd, 2);
//		System.out.println(cutZdd.count());
//		System.out.println(MyZDDPrinter.getPaths(cutZdd, zddManager));
		


		int[] a = {1};
		MyZDDNode zdd1 = zddManager.createPath(a);
		Tuple2<MyZDDNode, MyZDDNode> tuple = zddManager.splitZDD(zdd1, 1);
		System.out.print(MyZDDPrinter.getPaths(tuple._1(), zddManager));
		System.out.print(" || ");
		System.out.println(MyZDDPrinter.getPaths(tuple._2(), zddManager));
		
		
//		Map<MyZDDNode, MyZDDNode> cut_nodes = zddManager.find(z123);
//		for(MyZDDNode cut_node: cut_nodes.keySet()){
//			System.out.println(cut_node.hashCode() + "\t" + cut_node.toString());
//		}
//		System.out.println();
//		
//		zddManager.cut(cut_nodes, z123);
//		System.out.println(cut_nodes);
//		MyZDDPrinter.printZDD(z123, zddManager);
//		System.out.println(MyZDDPrinter.getPaths(z123, zddManager));
//		System.out.println();
////		for(MyZDDNode cut_node: cut_nodes.keySet()){
////			MyZDDPrinter.printZDD(cut_node, zddManager);
////			System.out.println(MyZDDPrinter.getPaths(cut_node, zddManager));
////			System.out.println();
////		}
//		
//		List<MyZDDNode> list = zddManager.append(cut_nodes);
//		MyZDDPrinter.printZDD(z123, zddManager);
//		System.out.println(MyZDDPrinter.getPaths(z123, zddManager));
//		System.out.println();
//		for(MyZDDNode cut_node: list){
//			MyZDDPrinter.printZDD(cut_node, zddManager);
//			System.out.println(MyZDDPrinter.getPaths(cut_node, zddManager));
//			System.out.println();
//		}
//		
//		MyZDDNode zproduct = zddManager.productSTFree(z12, z34);
//		System.out.println(MyZDDPrinter.getPaths(zproduct, zddManager));
//		MyZDDPrinter.printZDD(zproduct, zddManager);
	}
	
}
