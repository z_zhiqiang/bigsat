package edu.uci.bigsat.datastructure.zbdd;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import edu.uci.bigsat.datastructure.MyZBDDClauseCollection;


public class MyZDDNode implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3125173554057466548L;
	
	private final int value;
	private MyZDDNode low;
	private MyZDDNode high;
	
	
	public MyZDDNode(int v, MyZDDNode ln, MyZDDNode hn){
		this.value = v;
		this.low = ln;
		this.high = hn;
	}
	
//	public void cleanNode(){
//		if(this.isONE() || this.isZERO()){
//			return;
//		}
//		
//		if(low != null){
//			low.cleanNode();
//			low = null;
//		}
//		
//		if(high != null){
//			high.cleanNode();
//			high = null;
//		}
//	}
	
	public int getValue() {
		return value;
	}


	public MyZDDNode getLow() {
		return low;
	}


	public MyZDDNode getHigh() {
		return high;
	}
	
	
//	public void setLow(MyZDDNode low) {
//		this.low = low;
//	}
//
//
//	public void setHigh(MyZDDNode high) {
//		this.high = high;
//	}


	public String toString(){
		return MyZDDPrinter.reverse(this.value) + ":" + System.identityHashCode(this.low) + "," + System.identityHashCode(this.high) + ":" + count();
	}

	public boolean isONE(){
		return value == -1;
	}
	
	public boolean isZERO(){
		return value == 0;
	}

	/** get the total number of paths under this node
	 * @return
	 */
	public final long count() {
	if (this.isONE()) {
		return 1;
	}
	if (this.isZERO()) {
		return 0;
	}
	return this.getLow().count() + this.getHigh().count();
}
	
//	private void writeObject(ObjectOutputStream oos) throws IOException {
//		long stime = System.currentTimeMillis();
////		System.out.println("write");
//		// default serialization
//		oos.defaultWriteObject();
//		// write the object
////		System.exit(0);
//		MyZBDDClauseCollection.time += System.currentTimeMillis() - stime;
//		System.out.println("node write:\t" + MyZBDDClauseCollection.time);
//	}
//
//	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
//		long stime = System.currentTimeMillis();
////		System.out.println("read");
//		// default deserialization
//		ois.defaultReadObject();
////		System.exit(0);
//		MyZBDDClauseCollection.time += System.currentTimeMillis() - stime;
//		System.out.println("node read:\t" + MyZBDDClauseCollection.time);
//	}

	public int hashCode(){
//		int result = 7;
//		result = 31 * result + this.value;
//		result = 31 * result + this.low_id;
//		result = 31 * result + this.h_id;
//		return result;
		return this.value + System.identityHashCode(this.low) + System.identityHashCode(this.high);
	}
	
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		
		MyZDDNode c = (MyZDDNode) obj;
		
		return this.value == c.value && this.low == c.low && this.high == c.high;
	}
	
	
//	//------------------------------------------ inner class -----------------------------------------------
//	static class Triple implements Serializable {
//		/**
//		 * 
//		 */
//		private static final long serialVersionUID = 667978373028298101L;
//		
//		final int value;
//		final int low_id;
//		final int high_id;
//		
//		public Triple(int v, int lid, int hid){
//			this.value = v;
//			this.low_id = lid;
//			this.high_id = hid;
//		}
//		
//		public int hashCode(){
////			int result = 7;
////			result = 31 * result + this.value;
////			result = 31 * result + this.low_id;
////			result = 31 * result + this.h_id;
////			return result;
//			return this.value + this.low_id + this.high_id;
//		}
//		
//		public boolean equals(Object obj){
//			if(this == obj){
//				return true;
//			}
//			
//			if((obj == null) || (obj.getClass() != this.getClass())){
//				return false;
//			}
//			
//			Triple c = (Triple) obj;
//			
//			return this.value == c.value && this.low_id == c.low_id && this.high_id == c.high_id;
//		}
//		
//		
//		public String toString(){
//			StringBuilder builder = new StringBuilder();
//			
//			builder.append(MyZDDPrinter.reverse(this.value)).append(":").append(this.low_id).append(":").append(this.high_id);
//			
//			return builder.toString();
//		}
//		
////		private void writeObject(ObjectOutputStream oos) throws IOException {
////			long stime = System.currentTimeMillis();
//////			System.out.println("write");
////			// default serialization
////			oos.defaultWriteObject();
////			// write the object
//////			System.exit(0);
////			MyZBDDClauseCollection.time += System.currentTimeMillis() - stime;
////			System.out.println("triple write:\t" + MyZBDDClauseCollection.time);
////		}
////
////		private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
////			long stime = System.currentTimeMillis();
//////			System.out.println("read");
////			// default deserialization
////			ois.defaultReadObject();
//////			System.exit(0);
////			MyZBDDClauseCollection.time += System.currentTimeMillis() - stime;
////			System.out.println("triple read:\t" + MyZBDDClauseCollection.time);
////		}
//	
//	}
	

}
