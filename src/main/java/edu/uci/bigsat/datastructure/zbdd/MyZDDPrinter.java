package edu.uci.bigsat.datastructure.zbdd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyZDDPrinter {

	public static int reverse(int var){
		return var % 2 == 0 ? var / 2 : 0 - (var + 1) / 2;
	}
	
	public static void printZDD(MyZDDNode zdd){
		if(zdd.isZERO()){
			System.out.println(". 0");
		}
		else if (zdd.isONE()){
			System.out.println(". 1");
		}
		else{
			Set<Integer> marked_set = new HashSet<Integer>();
			printZDD_rec(zdd, marked_set);
			marked_set.clear();
			System.out.println();
		}
		
	}
	
	private static void printZDD_rec(MyZDDNode zdd, Set<Integer> marked_set){
		if(zdd.isZERO() || zdd.isONE()){
			return;
		}
		if(marked_set.contains(System.identityHashCode(zdd))){
			return;
		}
		System.out.println(System.identityHashCode(zdd) + ".\t (" + reverse(zdd.getValue()) + "):\t" + System.identityHashCode(zdd.getLow()) + ",\t" + System.identityHashCode(zdd.getHigh()));
		marked_set.add(System.identityHashCode(zdd));
		printZDD_rec(zdd.getLow(), marked_set);
		if(zdd.getLow() != zdd.getHigh()){
			printZDD_rec(zdd.getHigh(), marked_set);
		}
	}
	
	
	public static List<List<Integer>> getPaths(MyZDDNode zdd, MyZDDManager zddManager){
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		if(zdd.isZERO()){
			return list;
		}
		else if (zdd.isONE()){
			List<Integer> inlist = new ArrayList<Integer>();
			list.add(inlist);
			return list;
		}
		else{
			List<Integer> innerlist = new ArrayList<Integer>();
			getPaths_rec(zdd, zddManager, list, innerlist);
			return list;
		}
	}

	private static void getPaths_rec(MyZDDNode zdd, MyZDDManager zddManager, List<List<Integer>> list, List<Integer> innerlist) {
		// TODO Auto-generated method stub
		if(zdd.isZERO()){
			return;
		}
		if(zdd.isONE()){
			list.add(innerlist);
			return;
		}
		
		getPaths_rec(zdd.getLow(), zddManager, list, new ArrayList<Integer>(innerlist));
		innerlist.add(reverse(zdd.getValue()));
		getPaths_rec(zdd.getHigh(), zddManager, list, new ArrayList<Integer>(innerlist));
	}
	
}
