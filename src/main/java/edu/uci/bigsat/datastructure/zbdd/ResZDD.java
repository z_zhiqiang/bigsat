
package edu.uci.bigsat.datastructure.zbdd;

import jdd.util.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.uci.bigsat.datastructure.AbstractClauseCollection;
import jdd.bdd.*;


/**
 * Base implementation for Zero-Suppressed Binary Decision Diagrams (Z-BDDs).
 * Z-BDDs are a special type of BDDs that are more suited for sparse sets. For
 * example, if you are working with a set that is most of the time empty, you
 * might want to use Z-BDDs instead of BDDs.
 * <p>
 * The base implementation will give you the core Z-BDD operation such as
 * change, diff and union. To get more operators, see the sub-classes.
 *
 * @see BDD
 * @see ZDD2
 * @see ZDDCSP
 */

// NOTE FOR DEVELOPERS:
// Zero-suppressed BDDs use INVERSE variable order: v_last at top, v_0 at bottom just above 0 and 1.
// To make implementation easier, v_0 has t_var of 0 while 0 and 1 are assigned t_var of -1.


/**
 * A modified version of ZDD used by BigSAT, mainly based on Philippe Chatalic and Laurent Simon's paper ("Multi-Resolution on Compressed Sets of Clauses")
 * Diffs from the original ZDD:
 * 	c1). ascending order of literals (i.e., t_var), default ordering is: -1, 1, -2, 2, -3, 3, ...
 * 	a1). createPath from a list of sorted literals 
 * 	a2). noSupset which is same as subsumed difference, i.e., Definition 2 in Philippe Chatalic and Laurent Simon's paper
 * 	a3). noSubsumption, Definition 3
 * 	a4). unionSubsumFree, Definition 4
 * 	a5). productSTFree, Definition 5
 * 
 * @author icuzzq
 *
 */
public class ResZDD extends NodeTable {
	private static final int CACHE_SUBSET0 = 0, CACHE_SUBSET1 = 1, CACHE_CHANGE = 2, CACHE_UNION = 3,
			CACHE_INTERSECT = 4, CACHE_DIFF = 5;
	private static final int CACHE_NOSUPSET = 6, CACHE_NOSUBSUMPTION = 7, CACHE_SUBSUMFREEUNION = 8, CACHE_SUBSUMFREEPRODUCT = 9;

	
	protected int num_vars;/** number of Z-BDD variables */
	private int node_count_int;/** internal variable used by nodeCount */
	
	private OptimizedCache unary_cache; // for unary stuff. (ZDD, variable, op) => ZDD
	private OptimizedCache binary_cache; // for binary operations. (ZDD, ZDD, op) => ZDD

	
	
	/**
	 * create a Z-BDD manager
	 * 
	 * @param nodesize
	 *            is the number of nodes initially allocated in the node-table
	 */
	public ResZDD(int nodesize) {
		this(nodesize, Configuration.DEFAULT_ZDD_CACHE_SIZE);
	}

	/**
	 * create a Z-BDD manager
	 * 
	 * @param nodesize
	 *            is the number of nodes initially allocated in the node-table
	 * @param cachesize
	 *            is the suggested cache size.
	 */

	public ResZDD(int nodesize, int cachesize) {
		super(nodesize);

		unary_cache = new OptimizedCache("unary", cachesize / Configuration.zddUnaryCacheDiv, 3, 1);
		binary_cache = new OptimizedCache("binary", cachesize / Configuration.zddBinaryCacheDiv, 3, 2);

		// FIXME:
		// we can do this, but it wont include the base-class (ZDD2, ZDDCSP etc) caches:
//		if (Options.profile_cache)
//			new jdd.bdd.debug.BDDDebugFrame(this);

		// yes, we know how deep our trees are and will call tree_depth_changed()!
		enableStackMarking();
	}

	// ---------------------------------------------------------------
	/** cleanup this ZDD, to release its memory and help GC */
	public void cleanup() {
		super.cleanup();
		binary_cache = null;
		unary_cache = null;
	}
	// ---------------------------------------------------------------

	protected void post_removal_callbak() {
		binary_cache.free_or_grow(this);
		unary_cache.free_or_grow(this);
	}

	/** Zero-suppressed MK operator */
	public final int mk(int i, int l, int h) {
		/* ZDD node elimination */
		if (h == 0)
			return l; 
		
		/* Tautology elimination */
		if((i + 1) % 2 == 0 && getVar(h) == i + 1)
			return l;
		
		return add(i, l, h);
	}

	// --------------------------------------------------------
	/** returns {} */
	public final int empty() {
		return 0;
	}

	/** returns {{}} */
	public final int base() {
		return 1;
	}
	
	// -----------------------------------------------
	/** var is a variable, NOT a tree */
	public final int subset1(int zdd, int var) {
//		if (var < 0 || var >= num_vars)
//			return -1; // INVALID VARIABLE!
		if(zdd < 2){
			return 0;
		}
		
		if (getVar(zdd) > var)
			return 0;
		if (getVar(zdd) == var)
			return getHigh(zdd);

		// ... else if(getVar(zdd) > var)

		if (unary_cache.lookup(zdd, var, CACHE_SUBSET1))
			return unary_cache.answer;
		int hash = unary_cache.hash_value;

		int l = work_stack[work_stack_tos++] = subset1(getLow(zdd), var);
		int h = work_stack[work_stack_tos++] = subset1(getHigh(zdd), var);
		l = mk(getVar(zdd), l, h);
		work_stack_tos -= 2;

		unary_cache.insert(hash, zdd, var, CACHE_SUBSET1, l);
		return l;
	}

	/** var is a variable, NOT a tree */
	public final int subset0(int zdd, int var) {
//		if (var < 0 || var >= num_vars)
//			return -1; // INVALID VARIABLE!
		if(zdd < 2){
			return zdd;
		}

		if (getVar(zdd) > var)
			return zdd;
		if (getVar(zdd) == var)
			return getLow(zdd);

		// ... else if(getVar(zdd) > var)
		if (unary_cache.lookup(zdd, var, CACHE_SUBSET0))
			return unary_cache.answer;
		int hash = unary_cache.hash_value;

		int l = work_stack[work_stack_tos++] = subset0(getLow(zdd), var);
		int h = work_stack[work_stack_tos++] = subset0(getHigh(zdd), var);
		l = mk(getVar(zdd), l, h);
		work_stack_tos -= 2;
		unary_cache.insert(hash, zdd, var, CACHE_SUBSET0, l);
		return l;
	}

	public final int union(int p, int q) {
		if(p == 0) return q;
		if(q == 0 || q == p) return p;
		if(p == 1){
			return insert_base(q);
		}
		if(q == 1){
			return insert_base(p);
		}
		
		if(getVar(p) < getVar(q)) return union(q,p);
		// NOT USEFULL HERE: if(p == 1) 	return insert_base(q);

		if(binary_cache.lookup(p, q, CACHE_UNION)) return binary_cache.answer;
		int hash = binary_cache.hash_value;

		int l;
		if(getVar(p) > getVar(q)) {
			l = work_stack[work_stack_tos++] = union(p, getLow(q));
			l = mk(getVar(q), l, getHigh(q));
			work_stack_tos--;
		} else {
			l = work_stack[work_stack_tos++] = union( getLow(p), getLow(q));
			int h = work_stack[work_stack_tos++] = union(getHigh(p), getHigh(q));
			l = mk( getVar(p), l, h);
			work_stack_tos -= 2;
		}

		binary_cache.insert(hash, p, q, CACHE_UNION, l);
		return l;
	}
	

	public final int intersect(int p, int q) {
		if (p == 0 || q == 0)
			return 0;
		if (q == p)
			return p;
		if (p == 1)
			return follow_low(q); // ADDED BY ARASH, NOT TESTED VERY MUCH YET, DONT NOW IF IT MAKES THINGS FASTER OR SLOWER!!
		if (q == 1)
			return follow_low(p); // (not in the original Minato paper)

		if (binary_cache.lookup(p, q, CACHE_INTERSECT))
			return binary_cache.answer;
		int hash = binary_cache.hash_value;

		int l = 0;
		if (getVar(p) < getVar(q))
			l = intersect(getLow(p), q);
		else if (getVar(p) > getVar(q))
			l = intersect(p, getLow(q));
		else {
			// else if getVar(p) == getVar(q)
			l = work_stack[work_stack_tos++] = intersect(getLow(p), getLow(q));
			int h = work_stack[work_stack_tos++] = intersect(getHigh(p), getHigh(q));
			l = mk(getVar(p), l, h);
			work_stack_tos -= 2;
		}

		binary_cache.insert(hash, p, q, CACHE_INTERSECT, l);
		return l;
	}

	

	//----[ misc, mostly early-termination stuff]------------------------------------
	
	/** returns the terminal value along the all-low path */
	public final int follow_low(int zdd) {
		while (zdd >= 2)
			zdd = getLow(zdd);
		return zdd;
	}

	/** returns the terminal value along the all-high path */
	public final int follow_high(int zdd) {
		while (zdd >= 2)
			zdd = getHigh(zdd);
		return zdd;
	}

	/** returns true if base {{}} is in X */
	public final boolean emptyIn(int X) {
		return (follow_low(X) == 1);
	}

	/** computes set U {base} */
	private final int insert_base(int set) {
		if(set < 2) return 1; // <-- the magic happens here
		int l = work_stack[work_stack_tos++] = insert_base(getLow(set));
		l = (getLow(set) == l) ? set : mk(getVar(set), l, getHigh(set));
		work_stack_tos--;
		return l;
	}

	// --- [ misc] ----------------------------------------------
	public final int count(int zdd) {
		if (zdd < 2)
			return zdd;
		return count(getLow(zdd)) + count(getHigh(zdd));
	}

	// ----[ node count ] ----------------------------
	/**
	 * compute the number of nodes in the tree (this function is currently
	 * somewhat slow)
	 */
	public int nodeCount(int zdd) {
		node_count_int = 0;
		nodeCount_mark(zdd);
		unmark_tree(zdd);
		return node_count_int;
	}

	/** recursively mark and count nodes, used by nodeCount */
	private final void nodeCount_mark(int zdd) {
		if (zdd < 2)
			return;

		if (isNodeMarked(zdd))
			return;
		mark_node(zdd);
		node_count_int++;
		nodeCount_mark(getLow(zdd));
		nodeCount_mark(getHigh(zdd));
	}


	// --- [ debug ] ----------------------------------------------
	/** show ZDD statistics */
	public void showStats() {
		super.showStats();
		unary_cache.showStats();
		binary_cache.showStats();
	}

	/** return the amount of internally allocated memory in bytes */
	public long getMemoryUsage() {
		long ret = super.getMemoryUsage();
		if (unary_cache != null)
			ret += unary_cache.getMemoryUsage();
		if (binary_cache != null)
			ret += binary_cache.getMemoryUsage();
		return ret;
	}

	public void print(int zdd) {
		ZDDPrinter.print(zdd, this, null);
	}

	public void printSet(int bdd) {
		ZDDPrinter.printSet(bdd, this, null);
	}

	
	
	//----------------------- code added for resolution ------------------------
	//---------------- methods --------------------
	
	public void getList(int bdd, List<List<Integer>> list) {
		ZDDPrinter.getSet(bdd, this, null, list);
	}
	
	/**initiate a path corresponding to a sorted list of integers
	 * @param list
	 * @return
	 */
	public final int createPath(int[] list){
		int last = 1;
		
		for (int index = list.length - 1; index >= 0; index--){
			int i = list[index];
			int j = i > 0 ? 2 * i : 2 * Math.abs(i) - 1;
			work_stack[work_stack_tos++] = last;
			last = mk(j, 0, last);
			work_stack_tos--;
		}
//		print(last);
		return last;
	}
	
	public final int createPath(List<Integer> list){
		int last = 1;
		
		for (int index = list.size() - 1; index >= 0; index--){
			int i = list.get(index);
			int j = i > 0 ? 2 * i : 2 * (0 - i) - 1;
			work_stack[work_stack_tos++] = last;
			last = mk(j, 0, last);
			work_stack_tos--;
		}
//		print(last);
		return last;
	}

	
	/**product of two ZBDDs while removing all the subsumed and tautological clauses 
	 * @param p
	 * @param q
	 * @return
	 */
	public final int productSTFree(int p, int q){
		if(p == 0 || q == 0)
			return 0;
		if(p == 1)
			return noSubsumption(q);
		if(q == 1)
			return noSubsumption(p);
		
		if(getVar(p) > getVar(q)) return productSTFree(q,p);
		
		if(binary_cache.lookup(p, q, CACHE_SUBSUMFREEPRODUCT))
			return binary_cache.answer;
		int hash = binary_cache.hash_value;
		int ret;
		
		if(isNegative(p)){
			if(isConsecutive(p)){//R1, R2 and R3 of Definition 5 in Laurent Simon's paper
				int a1 = getHigh(p);
				int a2 = getHigh(getLow(p));
				int a3 = getLow(getLow(p));
				
				if(hasEqualVar(p, q)){//R1 and R2 
					if(isConsecutive(q)){//R1: (-x, (x, A3, A2), A1) * (-x, (x, B3, B2), B1)
						int b1 = getHigh(q);
						int b2 = getHigh(getLow(q));
						int b3 = getLow(getLow(q));
						
						int tmpa3b3 = work_stack[work_stack_tos++] = productSTFree(a3, b3);
						int tmpa1b1 = work_stack[work_stack_tos++] = productSTFree(a1, b1);
						int tmpa1b3 = work_stack[work_stack_tos++] = productSTFree(a1, b3);
						int tmpb1a3 = work_stack[work_stack_tos++] = productSTFree(b1, a3);
						int tmpr1 = noSupset(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b3), tmpb1a3), tmpa3b3);
						work_stack_tos -= 3;
						work_stack[work_stack_tos++] = tmpr1;
						
						int tmpa2b2 = work_stack[work_stack_tos++] = productSTFree(a2, b2);
						int tmpa2b3 = work_stack[work_stack_tos++] = productSTFree(a2, b3);
						int tmpb2a3 = work_stack[work_stack_tos++] = productSTFree(b2, a3);
						int tmpr2 = noSupset(unionSubsumFree(unionSubsumFree(tmpa2b2, tmpa2b3), tmpb2a3), tmpa3b3);
						work_stack_tos -= 3;
						work_stack[work_stack_tos++] = tmpr2;
						
						ret = mk(getVar(p), mk(getVar(getLow(p)), tmpa3b3, tmpr2), tmpr1);
						work_stack_tos -= 3;
						
						binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
						return ret;
					}
					else{//R2: (-x, (x, A3, A2), A1) * (-x, B2, B1) s.t. getVar(B2) != x
						int b1 = getHigh(q);
						int b2 = getLow(q);
						
						int tmpa3b2 = work_stack[work_stack_tos++] = productSTFree(a3, b2);
						int tmpa1b1 = work_stack[work_stack_tos++] = productSTFree(a1, b1);
						int tmpa1b2 = work_stack[work_stack_tos++] = productSTFree(a1, b2);
						int tmpb1a3 = work_stack[work_stack_tos++] = productSTFree(b1, a3);
						int tmpr1 = noSupset(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa1b2), tmpb1a3), tmpa3b2);
						work_stack_tos -= 3;
						work_stack[work_stack_tos++] = tmpr1;
						
						int tmpa2b2 = work_stack[work_stack_tos++] = productSTFree(a2, b2);
						int tmpr2 = noSupset(tmpa2b2, tmpa3b2);
						work_stack_tos--;
						work_stack[work_stack_tos++] = tmpr2;
						
						ret = mk(getVar(p), mk(getVar(getLow(p)), tmpa3b2, tmpr2), tmpr1);
						work_stack_tos -= 3;
						
						binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
						return ret;
					}
				}
				else if(hasConsecutiveVar(p, q)){//R3: (-x, (x, A3, A2), A1) * (x, B2, B1)
					int b1 = getHigh(q);
					int b2 = getLow(q);
					
					int tmpa3b2 = work_stack[work_stack_tos++] = productSTFree(a3, b2);
					int tmpa1b2 = work_stack[work_stack_tos++] = productSTFree(a1, b2);
					int tmpr1 = noSupset(tmpa1b2, tmpa3b2);
					work_stack_tos--;
					work_stack[work_stack_tos++] = tmpr1;
					
					int tmpa2b1 = work_stack[work_stack_tos++] = productSTFree(a2, b1);
					int tmpa2b2 = work_stack[work_stack_tos++] = productSTFree(a2, b2);
					int tmpb1a3 = work_stack[work_stack_tos++] = productSTFree(b1, a3);
					int tmpr2 = noSupset(unionSubsumFree(unionSubsumFree(tmpa2b1, tmpa2b2), tmpb1a3), tmpa3b2);
					work_stack_tos -= 3;
					work_stack[work_stack_tos++] = tmpr2;
					
					ret = mk(getVar(p), mk(getVar(getLow(p)), tmpa3b2, tmpr2), tmpr1);
					work_stack_tos -= 3;
					
					binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
					return ret;
				}
				
			}
			else if(hasEqualVar(p, q) && isConsecutive(q)){//symmetry of R2
				return productSTFree(q,p);
			}
			else if(hasConsecutiveVar(p, q)){//R4: (-x, A2, A1) * (x, B2, B1) s.t. getVar(A2) != x
				int a1 = getHigh(p);
				int a2 = getLow(p);
				int b1 = getHigh(q);
				int b2 = getLow(q);
				
				int tmpa2b2 = work_stack[work_stack_tos++] = productSTFree(a2, b2);
				int tmpa1b2 = work_stack[work_stack_tos++] = productSTFree(a1, b2);
				int tmpr1 = noSupset(tmpa1b2, tmpa2b2);
				work_stack_tos--;
				work_stack[work_stack_tos++] = tmpr1;
				
				int tmpa2b1 = work_stack[work_stack_tos++] = productSTFree(a2, b1);
				int tmpr2 = noSupset(tmpa2b1, tmpa2b2);
				work_stack_tos--;
				work_stack[work_stack_tos++] = tmpr2;
				
				ret = mk(getVar(p), mk(getVar(q), tmpa2b2, tmpr2), tmpr1);
				work_stack_tos -= 3;
				
				binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
				return ret;
			}
		}
		
		
		if(hasEqualVar(q, p)){//R5
			int a1 = getHigh(p);
			int a2 = getLow(p);
			int b1 = getHigh(q);
			int b2 = getLow(q);
			
			int tmpa2b2 = work_stack[work_stack_tos++] = productSTFree(a2, b2);
			int tmpa1b1 = work_stack[work_stack_tos++] = productSTFree(a1, b1);
			int tmpa2b1 = work_stack[work_stack_tos++] = productSTFree(a2, b1);
			int tmpa1b2 = work_stack[work_stack_tos++] = productSTFree(a1, b2);
			int tmprh = noSupset(unionSubsumFree(unionSubsumFree(tmpa1b1, tmpa2b1), tmpa1b2), tmpa2b2);
			work_stack_tos -= 3;
			work_stack[work_stack_tos++] = tmprh;
			
			ret = mk(getVar(p), tmpa2b2, tmprh);
			work_stack_tos -= 2;
			
			binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
			return ret;
		}
		else{//R6 and R7 which are symmetry
			int tmpa2q = work_stack[work_stack_tos++] = productSTFree(getLow(p), q);
			int tmpa1q = work_stack[work_stack_tos++] = productSTFree(getHigh(p), q);
			int tmprh = noSupset(tmpa1q, tmpa2q);
			work_stack_tos--;
			work_stack[work_stack_tos++] = tmprh;
			
			ret = mk(getVar(p), tmpa2q, tmprh);
			work_stack_tos -=2;
			
			binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEPRODUCT, ret);
			return ret;
		}
		
		
	}


	/**p and q have consecutive label values, i.e., p = (-x, plow_, phigh_), q = (x, qlow_, qhigh_)
	 * @param p
	 * @param q
	 * @return
	 */
	private boolean hasConsecutiveVar(int p, int q) {
		return getVar(q) == (getVar(p) + 1);
	}


	/**p and q have same label values, i.e., p = (l, plow_, phigh_), q = (l, qlow_, qhigh_)
	 * @param p
	 * @param q
	 * @return
	 */
	private boolean hasEqualVar(int p, int q) {
		return getVar(q) == getVar(p);
	}


	/**p's low has consecutive values with p, i.e., p = (-x, plow_, (x, phigh_low_, phigh_high_))
	 * @param p
	 * @return
	 */
	private boolean isConsecutive(int p) {
		return getVar(getLow(p)) == (getVar(p) + 1);
	}


	/**p's value is negative, i.e., p = (-x, plow_, phigh_)
	 * @param p
	 * @return
	 */
	private boolean isNegative(int p) {
		return (getVar(p) + 1) % 2 == 0;
	}
	
	
	
	/** Subsumption-free union, Definition 4 in Laurent Simon's paper
	 * @param p
	 * @param q
	 * @return
	 */
	public final int unionSubsumFree(int p, int q){
		if(p == 0){
			return noSubsumption(q); 
		}
		if(q == 0 || p == q){
			return noSubsumption(p);
		}
		if(p == 1 || q == 1){
			return 1;
		}

		if(getVar(p) < getVar(q)){
			return unionSubsumFree(q, p);
		}
		
		if(binary_cache.lookup(p, q, CACHE_SUBSUMFREEUNION))
			return binary_cache.answer;
		int hash = binary_cache.hash_value;
		
		int tmpl, tmph;
		int ret;
		if(getVar(p) > getVar(q)){
			tmpl = work_stack[work_stack_tos++] = unionSubsumFree(getLow(q), p);
			tmph = work_stack[work_stack_tos++] = noSupset(noSubsumption(getHigh(q)), tmpl);
			ret = mk(getVar(q), tmpl, tmph);
			work_stack_tos -= 2;
		}
		else{
			tmpl = work_stack[work_stack_tos++] = unionSubsumFree(getLow(p), getLow(q));
			tmph = work_stack[work_stack_tos++] = unionSubsumFree(getHigh(p), getHigh(q));
			tmph = noSupset(tmph, tmpl);
			work_stack_tos--;
			work_stack[work_stack_tos++] = tmph;
			ret = mk(getVar(p), tmpl, tmph);
			work_stack_tos -= 2;
		}

		binary_cache.insert(hash, p, q, CACHE_SUBSUMFREEUNION, ret);
		return ret;
	}
	
	
	/** Subsumption elimination, Definition 3 in Laurent Simon's paper
	 * @param zdd
	 * @return
	 */
	public final int noSubsumption(int zdd){
		if(zdd == 1)
			return 1;
		if(zdd == 0)
			return 0;
		
		if(unary_cache.lookup(zdd, CACHE_NOSUBSUMPTION)) return unary_cache.answer;
		int hash = unary_cache.hash_value;
		
		int low = getLow(zdd);
		int high = getHigh(zdd);
		int ret;
		if(low == high){
			ret = noSubsumption(low);
		}
		else{
			int tmpl = work_stack[work_stack_tos++] = noSubsumption(low);
			int tmph = work_stack[work_stack_tos++] = noSubsumption(high);
			tmph = noSupset(tmph, tmpl);
			work_stack_tos--;
			work_stack[work_stack_tos++] = tmph;
			ret = mk(getVar(zdd), tmpl, tmph);
			work_stack_tos -= 2;
		}
		
		unary_cache.insert(hash, zdd, CACHE_NOSUBSUMPTION, ret);
		return ret;
		
	}
	
	
    /**
	 * noSupset which is a same functionality as subsumed difference, i.e., Definition 2 in Laurent Simon's paper
	 *
	 * noSupset(F, C) = {f \in F | \lnot \exist c \in C. c \subseteq f }
     */
    public int noSupset(int F, int C)  {
		if( emptyIn(C)) return 0;
		return noSupset_rec(F, C);
	}


    private final int noSupset_rec(int F, int C) {
        
		if(F == 0 || C == 1 || F == C) return 0;
        if(F == 1 || C == 0) return F;         
        
		if(binary_cache.lookup(F, C, CACHE_NOSUPSET)) return binary_cache.answer;
		int hash = binary_cache.hash_value;

		int ret;
		int fvar = getVar(F);
		int cvar = getVar(C);

		if (fvar > cvar) {
			ret = noSupset_rec(F, getLow(C));
		} else if (fvar < cvar) {
			int tmp1 = work_stack[work_stack_tos++] = noSupset_rec(getHigh(F), C);
			int tmp2 = work_stack[work_stack_tos++] = noSupset_rec(getLow(F) , C);
			ret = mk(fvar, tmp2, tmp1);
			work_stack_tos -= 2;
		} else {
            
            int tmp1, tmp2;
            int C1 = getHigh(C);
            
            if( emptyIn(C1)) { 
                // special case, beause  noSupset( getHigh(F), C1) = 0
                work_stack[work_stack_tos++] = tmp1 = 0;
            } else {
                tmp1 = work_stack[work_stack_tos++] = noSupset_rec( getHigh(F), getLow(C));
                tmp2 = work_stack[work_stack_tos++] = noSupset_rec( getHigh(F), C1);                
                tmp1 = intersect(tmp1, tmp2);
                work_stack_tos -= 2;
                work_stack[work_stack_tos++] = tmp1;
            }


			tmp2 = work_stack[work_stack_tos++] = noSupset_rec( getLow(F), getLow(C));
			ret = mk(fvar, tmp2, tmp1);
			work_stack_tos -= 2;
		}

		binary_cache.insert(hash, F, C, CACHE_NOSUPSET, ret);
		return ret;
    }
	
    
    public final int rebuildZBDD(int zdd, ResZDD zddC){
    	int retZdd;
    	if(zdd == 0 || zdd == 1){
    		return zdd;
    	}
    	
//    	if(zddC.getHigh(zdd) == 1 && zddC.getLow(zdd) == 0){
//    		return mk(zddC.getVar(zdd), zddC.getLow(zdd), zddC.getHigh(zdd));
//    	}
    	
    	int low = rebuildZBDD(zddC.getLow(zdd), zddC);
    	int high = rebuildZBDD(zddC.getHigh(zdd), zddC);
    	retZdd = mk(zddC.getVar(zdd), low, high);
    	
    	return retZdd;
    }
    
	//----------------------- code added for resolution ------------------------
		
    
    
    
	public static void main(String[] args) {
//		ResZDD zddClass1 = new ResZDD(100);
//		int[] a1 = {1, 2, 4, 6, 7, 8};
//		int z1 = zddClass1.createPath(a1);
//		zddClass1.print(z1);
//		
//		ResZDD zddClass2 = new ResZDD(100);
//		int[] a2 = {1, 2, 5, 7, 8};
//		int z2 = zddClass2.createPath(a2);
//		zddClass2.print(z2);
//		
//		int zz2 = zddClass1.rebuildZBDD(z2, zddClass2);
//		zddClass1.print(zz2);
//		
//		
//		
//		
//		
//		int mixzdd = zddClass1.union(z1, zz2);
//		zddClass1.print(mixzdd);
//		zddClass1.printSet(mixzdd);
		
		ResZDD zdd = new ResZDD(10000);
//		
//		int base = zdd.base();
//		System.out.println(zdd.getLow(base));
//		
//		
		int[] a1 = {2, 3, -8, 10, 31, -40};
		int[] a2 = {1, -8};
		int[] a3 = {1, 2, 4, 8};
		int[] a4 = {4, -5, -9};

		int z1 = zdd.createPath(a1);
		int z2 = zdd.createPath(a2);
		int z3 = zdd.createPath(a3);
		int z4 = zdd.createPath(a4);
//		int retZdd = zdd.union(z1, z2);
//		zdd.print(retZdd);
////		
//		int zproduct = zdd.productSTFree(z1, z2);
//		System.out.println(zproduct);
//		zdd.print(zproduct);
//		zdd.printSet(zproduct);
//		
//		retZdd = zdd.union(retZdd, z3);
//		zdd.printSet(retZdd);
//		List<List<Integer>> list = new ArrayList<List<Integer>>();
//		ZDDPrinter.getSet(retZdd, zdd, null, list);
//		System.out.println(list);
		
//		do{
//			int newzdd = zdd.mk(zdd.getVar(retZdd), 0, zdd.getHigh(retZdd));
//			zdd.printSet(newzdd);
//			zdd.print(newzdd);
////			new ZBDDClauseCollection(1, newzdd).printZBDDs();
//			
//			
//			retZdd = zdd.getLow(retZdd);
//		}
//		while(retZdd != 0);
		
//		int z3 = zdd.createPath(a3);
//		int z4 = zdd.createPath(a4);
//		zdd.printSet(z1);
//		zdd.printSet(z2);
//		zdd.printSet(z3);
//		zdd.printSet(z4);
////		zdd.print(z1);
////		zdd.print(z2);
////		zdd.print(z3);
////		zdd.print(z4);
//		
		int z12 = zdd.union(z1, z2);
		zdd.printSet(z12);
		zdd.print(z12);
		
		int z34 = zdd.union(z3, z4);
		zdd.printSet(z34);
		zdd.print(z34);
		
		int zproduct = zdd.productSTFree(z12, z34);
		zdd.printSet(zproduct);
		zdd.print(zproduct);
		
//		int[] ano1 = {2};
//		int zno1 = zdd.createPath(ano1);
//		zdd.printSet(zdd.noSubsumption(zno1));
//		
//		int[] az1 = { -23, -24, 29, -34, 36, -40, 41, 43, -46, -50, 67};
//		int azdd1 = zdd.createPath(az1);
//		int[] az25 = { -19, -33, -34, -35, -37, 41, 43, -44, -46, -47 };
//		int azdd25 = zdd.createPath(az25);
//
//		int azproduct = zdd.productSTFree(azdd1, azdd25);
//		zdd.printSet(azproduct);

//		int[] az21 = { 19, -34, -35, 41, -43, -44, -46, -50 };
//		int[] az22 = { 19, -34, -35, -37, 41, 43, -44, -46, -50 };
//		int[] az23 = { 19, -34, -35, -36, -40, 41, -44, -46, -50 };
//		int[] az24 = { -19, -33, -34, -35, 41, -43, -44, -46, -47 };
//		int[] az26 = { -19, -33, -34, -35, -36, -40, 41, -44, -46, -47 };
//		int azdd21 = zdd.createPath(az21);
//		int azdd22 = zdd.createPath(az22);
//		int azdd23 = zdd.createPath(az23);
//		int azdd24 = zdd.createPath(az24);
//		int azdd26 = zdd.createPath(az26);
//
//		int azdd2 = zdd
//				.unionSubsumFree(
//						zdd.unionSubsumFree(zdd.unionSubsumFree(
//								zdd.unionSubsumFree(zdd.unionSubsumFree(azdd21, azdd22), azdd23), azdd24), azdd25),
//						azdd26);
	}
}
