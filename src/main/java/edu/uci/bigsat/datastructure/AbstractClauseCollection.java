package edu.uci.bigsat.datastructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.spark.Accumulator;
import org.apache.spark.broadcast.Broadcast;

import scala.Tuple2;


public abstract class AbstractClauseCollection implements Serializable {
	
	//iteration number to indicate from which computation iteration this clauseCollection is generated
	private final int iteration_number;
	
	
	public AbstractClauseCollection(int iteration){
		this.iteration_number = iteration;
	}

	public int getIteration_number() {
		return iteration_number;
	}
	
	public void clean(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
	}
	/*----------------------------------------------------------- methods to be overridden ----------------------------------------------------------------*/
	
//	//add one clause into this clauseCollection
//	public static AbstractClauseCollection addClause(AbstractClauseCollection clauses, BucketClause clause){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//	
//	//add another clauseCollection into this clauseCollection
//	public static AbstractClauseCollection addClauses(AbstractClauseCollection clauses){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//	
//	//do resolution between this clauseCollection and another clauseCollection to generate a list of clauses
//	public static List<BucketClause> doResolution(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
	
	//test if this clauseCollection is empty
	public boolean isEmpty(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
	}

//	//do resolution between this clauseCollection and another clauseCollection to generate a list of clauseCollections
//	public List<AbstractClauseCollection> doResolutionCollection(AbstractClauseCollection clauses, Accumulator<Integer> accumulator, int iteration_num){
//		System.err.println("Empty method executed!!!");
//		return null;
//	}
	
	//get the number of clauses
	public long getNumClauses(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
//		return null;
	}
	

	public Integer getKey(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
//		return null;
	}
	
	public boolean isPositive(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
//		return null;
	}
	
	public boolean isNegative(){
		System.err.println("Empty method executed!!!");
		throw new RuntimeException("Empty method executed!!!");
//		return null;
	}


//	public static AbstractClauseCollection doResolutionCollection(AbstractClauseCollection one_clauses,  AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//
//	public static List<Tuple2<Integer, AbstractClauseCollection>> splitBuckets(AbstractClauseCollection one_clauses, int iteration_num){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}

//	//do resolution on the pair of clauseCollections: <this, clauses>
//	public List<Tuple2<Integer, AbstractClauseCollection>> doResolutionPair(AbstractClauseCollection clauses, Accumulator<Integer> accumulator, final int iteration_num){
//		System.err.println("Empty method executed!!!");
//		return null;
//	}

//	//rename a portion of this clauseCollection
//	public List<Tuple2<Integer, AbstractClauseCollection>> renamePartialCollection(int number_buckets, long flag){
//		System.err.println("Empty method executed!!!");
//		return null;
//	}

//	//do resolution on the pair of clauseCollections: <this, clauses>, while balancing workload by renaming a portion of clauseCollections
//	public List<Tuple2<Integer, AbstractClauseCollection>> doResolutionPair_balance(AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator, int iteration_num, double percentage, int number_buckets){
//		System.err.println("Empty method executed!!!");
//		return null;
//	}

//	//rename a portion of clauseCollections pair <this, clauses>, and combine into one clauseCollection
//	public AbstractClauseCollection renameAndcombinePair(AbstractClauseCollection clauses, double percentage, int number_buckets){
//		System.err.println("Empty method executed!!!");
//		return null;
//	}

//	//combine the pair of clauseCollections: <this, clauses> into one clauseCollection
//	public static AbstractClauseCollection combinePair(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses){
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}

//	//count the number of resolutions need to be performed on the pair of clauseCollections: <this, clauses>
//	public long countResolutions(AbstractClauseCollection clauses){
//		System.err.println("Empty method executed!!!");
//		return 0;
//	}


//	public static List<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>> splitResolution(AbstractClauseCollection one_clauses, 
//			AbstractClauseCollection clauses, int num_splits) {
//		// TODO Auto-generated method stub
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//
//
//	public static Tuple2<AbstractClauseCollection, AbstractClauseCollection> removeDuplicates(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses) {
//		// TODO Auto-generated method stub
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//
//
//	public static AbstractClauseCollection doResolutionCollection_noFirst(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator) {
//		// TODO Auto-generated method stub
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
//	
//	public static List<Tuple2<Integer, AbstractClauseCollection>> doResolutionCollection_noFirst(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator, int iteration_num) {
//		// TODO Auto-generated method stub
//		System.err.println("Empty method executed!!!");
//		throw new RuntimeException("Empty method executed!!!");
////		return null;
//	}
	
}
