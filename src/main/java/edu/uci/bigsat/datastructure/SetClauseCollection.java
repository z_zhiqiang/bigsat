package edu.uci.bigsat.datastructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.spark.Accumulator;
import org.apache.spark.broadcast.Broadcast;

import edu.uci.bigsat.BigSAT;
import edu.uci.bigsat.datastructure.zbdd.MyZDDManager;
import edu.uci.bigsat.datastructure.zbdd.MyZDDNode;
import edu.uci.bigsat.datastructure.zbdd.MyZDDPrinter;
import edu.uci.bigsat.datastructure.zbdd.ResZDD;
import scala.Tuple2;


public class SetClauseCollection extends AbstractClauseCollection {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6507154837668420051L;
	
	public static int index = 100;
	
	private final Set<BucketClause> set_clauses;
	
	
	public SetClauseCollection(int iteration){
		super(iteration);
		this.set_clauses = new HashSet<BucketClause>();
	}
	
	public SetClauseCollection(int iteration, Set<BucketClause> set){
		super(iteration);
		this.set_clauses = set;
	}

	public static AbstractClauseCollection addClause(AbstractClauseCollection one_clauses, BucketClause clause){
		Set<BucketClause> set = new HashSet<BucketClause>(); 
		set.addAll(((SetClauseCollection) one_clauses).set_clauses);
		set.add(clause);
		return new SetClauseCollection(one_clauses.getIteration_number(), set);
		
	}
	
	public static AbstractClauseCollection addClauses(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses){
		Set<BucketClause> set = new HashSet<BucketClause>(); 
		set.addAll(((SetClauseCollection) one_clauses).set_clauses);
		set.addAll(((SetClauseCollection) clauses).set_clauses);
		return new SetClauseCollection(one_clauses.getIteration_number(), set);
	}
	
	
//	/** set implementation
//	 * @param clauses
//	 * @param accumulator
//	 * @return
//	 */
//	@Override
//	public List<BucketClause> doResolution(AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
//		SetClauseCollection firstClauses = this;
//		SetClauseCollection secondClauses = (SetClauseCollection) clauses;
//		
//		//generate vertical information: for each literal, a list of clauses containing this literal 
//		Map<Integer, ArrayList<Integer>> verticalInfo = new HashMap<Integer, ArrayList<Integer>>();
//		BucketClause[] clausesArray = new BucketClause[secondClauses.set_clauses.size()];
//		int index = 0;
//		for(BucketClause clause: secondClauses.set_clauses){
//			clausesArray[index] = clause;
//			for(int literal: clause.literals){
//				if(verticalInfo.containsKey(literal)){
//					verticalInfo.get(literal).add(index);
//				}
//				else{
//					ArrayList<Integer> idList = new ArrayList<Integer>(); 
//					idList.add(index);
//					verticalInfo.put(literal, idList);
//				}
//			}
//			
//			index++;
//		}
//		verticalInfo.remove(secondClauses.getKeyLiteral());
//		
//		
//		Set<BucketClause> set = new HashSet<BucketClause>();
//		for(BucketClause firstC : firstClauses.set_clauses){
//			//get a set of clause indices whose resolvent with firstC would be tautological clause.
//			Set<Integer> trueSet = new HashSet<Integer>();
//			for(int literal: firstC.literals){
//				if(verticalInfo.containsKey(0 - literal)){
//					trueSet.addAll(verticalInfo.get(0 - literal));
//				}
//			}
//			
//			//do resolution with necessary clauses
//			for(int i = 0; i < clausesArray.length; i++){
//				if(!trueSet.contains(i)){
//					BucketClause newC =  new BucketClause(firstC, clausesArray[i]);
//					set.add(newC);
//				}
//				
//				if(firstC.isUnitClause() && clausesArray[i].isUnitClause()){
//					accumulator.add(1);
//					return new ArrayList<BucketClause>();
//				}
//			}
//		}
//		
//		return new ArrayList<BucketClause>(set);
//	}
	
//	@Override
//	public List<BucketClause> doResolution(AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
//		
//		Set<BucketClause> set = new HashSet<BucketClause>();
//		
//		for(BucketClause firstC : this.set_clauses){
//			for(BucketClause secondC: clauses.set_clauses){
//				BucketClause newC =  new BucketClause(firstC, secondC);
//				if(newC.isFalse()){
//					accumulator.add(1);
//				}
//				else if(!newC.isTrue()){
//					set.add(newC);
//				}
//			}
//		}
//		
//		return new ArrayList<BucketClause>(set);
//	}
	
	
	public static List<BucketClause> doResolution(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
		List<BucketClause> list = new ArrayList<BucketClause>();
		
		//initialize a zdd manager
		ResZDD zddManager = new ResZDD(100);
		
//		long stime = System.currentTimeMillis();
		int zdd1 = buildZDDs(zddManager, one_clauses);
		int zdd2 = buildZDDs(zddManager, clauses);
//		long btime = System.currentTimeMillis();
//		System.out.println("Building time:\t" + (btime - stime));
		
		//remove the first literal before resolution
		zdd1 = zddManager.getHigh(zdd1);
		zdd2 = zddManager.getHigh(zdd2);
		//two unit clauses generating empty clause
		if(zdd1 == 1 && zdd2 == 1){
			accumulator.add(1);
			return list;
		}
		
		//resolution
		int retZdd = zddManager.productSTFree(zdd1, zdd2);
//		BigSAT.tcount += zddManager.count(retZdd);
//		System.out.println("Product time:\t" + (System.currentTimeMillis() - btime));
		
		assert(retZdd != 1);
		if(retZdd < 2){
			return list;
		}
		
		//generate a list of clauses
		List<List<Integer>> arrayList = new ArrayList<List<Integer>>();
		zddManager.getList(retZdd, arrayList);
		for(List<Integer> alist: arrayList){
			if(alist.isEmpty()){
				continue;
			}
			
			BucketClause c = new BucketClause(alist);
			list.add(c);
			
		}
		
		return list;
	}
	
	
//	public List<BucketClause> doResolution(AbstractClauseCollection clauses, Accumulator<Integer> accumulator){
//		List<BucketClause> list = new ArrayList<BucketClause>();
//		
//		//initialize a new zdd manager
//		MyZDDManager zddManager = new MyZDDManager();
//				
//		//rebuild this.zdd and another zdd based on the new zdd manager
//		MyZDDNode zdd = buildZDDs(zddManager, this);
//		MyZDDNode anotherZdd = buildZDDs(zddManager, clauses);
//		
//		MyZDDNode zdd1 = zdd.getHigh();
//		MyZDDNode zdd2 = anotherZdd.getHigh();
//		//two unit clauses generating empty clause
//		if(zdd1.isONE() && zdd2.isONE()){
//			accumulator.add(1);
//			return list;
//		}
//		
//		//resolution
//		MyZDDNode retZdd = zddManager.productSTFree(zdd1, zdd2);
//		BigSAT.tcount += retZdd.count();
//		
//		assert(!retZdd.isONE());
//		if(retZdd.isZERO() || retZdd.isONE()){
//			return list;
//		}
//		
//		//generate a list of clauses
//		List<List<Integer>> arrayList = MyZDDPrinter.getPaths(retZdd, zddManager);
//		for(List<Integer> alist: arrayList){
//			if(alist.isEmpty()){
//				continue;
//			}
//			
//			BucketClause c = new BucketClause(alist);
//			list.add(c);
//			
//		}
//		
//		return list;
//	}

	
	public void printArray(int[] a){
		System.out.print("[");
		for(int i: a){
			System.out.print(i + " "); 
		}
		System.out.println("]");
	}
	
	private MyZDDNode buildZDDs(MyZDDManager zddManager, AbstractClauseCollection setClauseCollection) {
		MyZDDNode zdd1 = zddManager.empty();
		for(BucketClause bc: ((SetClauseCollection) setClauseCollection).set_clauses){
			MyZDDNode zddtmp = zddManager.createPath(bc.getLiterals());
			zdd1 = zddManager.unionSubsumFree(zdd1, zddtmp);
		}
		return zdd1;
	}
	
	/**build a zdd representing a set of clauses
	 * @param zddManager
	 * @param setClauseCollection
	 * @return
	 */
	private static int buildZDDs(ResZDD zddManager, AbstractClauseCollection setClauseCollection) {
		int zdd1 = zddManager.empty();
		for(BucketClause bc: ((SetClauseCollection) setClauseCollection).set_clauses){
			int zddtmp = zddManager.createPath(bc.getLiterals());
			zdd1 = zddManager.unionSubsumFree(zdd1, zddtmp);
		}
		return zdd1;
	}

	public int getKeyLiteral(){
		BucketClause c = this.set_clauses.iterator().next();
		return c.getLiterals().get(0);
	}
	
	
	@Override
	public boolean isEmpty() {
		return this.set_clauses.isEmpty();
	}

	
	public Set<BucketClause> getSet_clauses() {
		return set_clauses;
	}
	
	
	@Override
	public Integer getKey() {
		// TODO Auto-generated method stub
		BucketClause c = this.set_clauses.iterator().next();
		return c.getKey();
	}
	

	@Override
	public long getNumClauses() {
		// TODO Auto-generated method stub
		return this.set_clauses.size();
	}


}
