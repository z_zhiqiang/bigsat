package edu.uci.bigsat.datastructure;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.spark.Accumulator;

import edu.uci.bigsat.BigSAT;
import edu.uci.bigsat.datastructure.zbdd.MyZDDManager;
import edu.uci.bigsat.datastructure.zbdd.MyZDDNode;
import edu.uci.bigsat.datastructure.zbdd.MyZDDPrinter;
import edu.uci.bigsat.datastructure.zbdd.ResZDD;
import scala.Tuple2;


public class MyZBDDClauseCollection extends AbstractClauseCollection {
/**
	 * 
	 */
	private static final long serialVersionUID = -7220202539075285868L;

	
	private MyZDDNode zdd;
	
	
	public MyZBDDClauseCollection(int iteration_number){
		super(iteration_number);
		this.zdd = MyZDDManager.ZERO;
	}
	
//	public MyZBDDClauseCollection(int iteration_number, int[] list){
//		super(iteration_number);
//		MyZDDManager zddManager = new MyZDDManager();
//		this.zdd = zddManager.createPath(list);
////		zddManager.cleanup();
//	}
	
	private MyZBDDClauseCollection(int iteration_number, MyZDDNode zddCollection){
		super(iteration_number);
		this.zdd = zddCollection;
	}
	
	private void writeObject(ObjectOutputStream oos) throws IOException {
		long stime = System.currentTimeMillis();
//		System.out.println("write");
		// default serialization
		oos.defaultWriteObject();
		BigSAT.serializetime += System.currentTimeMillis() - stime;
//		System.out.println("write:\t" + time);
	}

	private void readObject(ObjectInputStream ois) throws ClassNotFoundException, IOException {
		long stime = System.currentTimeMillis();
//		System.out.println("read");
		// default deserialization
		ois.defaultReadObject();
		BigSAT.serializetime += System.currentTimeMillis() - stime;
//		System.out.println("read:\t" + time);
	}
	
	public void clean(){
		this.zdd = null;
	}

	public static AbstractClauseCollection addClause(AbstractClauseCollection one_clauses, BucketClause clause) {
		MyZDDNode zddclause = MyZDDManager.createPath_static(clause.getLiterals());
		MyZDDManager zddManager = new MyZDDManager();
		MyZDDNode retZdd = zddManager.unionSubsumFree(((MyZBDDClauseCollection) one_clauses).zdd, zddclause);
		zddManager.cleanup();
		one_clauses.clean();
		
		return new MyZBDDClauseCollection(one_clauses.getIteration_number(), retZdd);
	}

	public static AbstractClauseCollection addClauses(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses) {
		MyZDDManager zddManager = new MyZDDManager();
		MyZDDNode retZdd = zddManager.unionSubsumFree(((MyZBDDClauseCollection) one_clauses).zdd, ((MyZBDDClauseCollection) clauses).zdd);
		zddManager.cleanup();
		one_clauses.clean();
		clauses.clean();
		
		return new MyZBDDClauseCollection(one_clauses.getIteration_number(), retZdd);
	}
	
	
	public static List<Tuple2<Integer, AbstractClauseCollection>> splitBuckets(AbstractClauseCollection one_clauses, int iteration_num) {
		// TODO Auto-generated method stub
		List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
		MyZDDNode zdd = ((MyZBDDClauseCollection) one_clauses).zdd;
		
		assert(!zdd.isONE());
		if(zdd.isZERO() || zdd.isONE()){
			return list;
		}
		
		MyZDDNode retZdd = zdd;
		//get a list of ZBDDs belonging to different buckets
		do{
			if (MyZDDManager.isNegConsecutive(retZdd)) {
				MyZDDNode newlowzdd = new MyZDDNode(retZdd.getLow().getValue(), MyZDDManager.ZERO, retZdd.getLow().getHigh());
				MyZDDNode newzdd = new MyZDDNode(retZdd.getValue(), newlowzdd, retZdd.getHigh());
				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
				retZdd = retZdd.getLow().getLow();
			} 
			else {
				MyZDDNode newzdd = new MyZDDNode(retZdd.getValue(), MyZDDManager.ZERO, retZdd.getHigh());
				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
				retZdd = retZdd.getLow();
			}
		}
		while(!retZdd.isZERO());
		
		return list;
	}
	

//	public AbstractClauseCollection doResolutionCollection_noFirst(AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator) {
//		MyZDDNode retZdd = doPair(zdd, ((MyZBDDClauseCollection) clauses).zdd, accumulator);
////		BigSAT.tcount += retZdd.count();
//		
//		return new MyZBDDClauseCollection(-1, retZdd);
//	}
	
	public static List<Tuple2<Integer, AbstractClauseCollection>> doResolutionCollection_noFirst(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses,
			Accumulator<Integer> accumulator, int iteration_num) {
		List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();

		MyZDDNode retZdd = doPair(((MyZBDDClauseCollection) one_clauses).zdd, ((MyZBDDClauseCollection) clauses).zdd, accumulator);
		BigSAT.tcount += retZdd.count();
		one_clauses.clean();
		clauses.clean();
		
		
		assert(!retZdd.isONE());
		if(retZdd.isZERO() || retZdd.isONE()){
			return list;
		}
		
		//get a list of ZBDDs belonging to different buckets
		do{
			if (MyZDDManager.isNegConsecutive(retZdd)) {
				MyZDDNode newlowzdd = new MyZDDNode(retZdd.getLow().getValue(), MyZDDManager.ZERO, retZdd.getLow().getHigh());
				MyZDDNode newzdd = new MyZDDNode(retZdd.getValue(), newlowzdd, retZdd.getHigh());
				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
				retZdd = retZdd.getLow().getLow();
			} 
			else {
				MyZDDNode newzdd = new MyZDDNode(retZdd.getValue(), MyZDDManager.ZERO, retZdd.getHigh());
				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
				retZdd = retZdd.getLow();
			}
		}
		while(!retZdd.isZERO());
		
		return list;
	}

	public static List<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>> splitResolution(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses, final int num_splits) {
		// TODO Auto-generated method stub
		long stime = System.currentTimeMillis();
		List<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>> list = new ArrayList<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>>();
		
		//initialize a new zdd manager
		MyZDDManager zddManager = new MyZDDManager();
		
		int pos_value = ((MyZBDDClauseCollection) one_clauses).getKey() * 2, neg_value = pos_value - 1;
		MyZDDNode new_neg = null, new_pos = null, old_neg = null, old_pos = null;

		//rebuild this.zdd based on the new zdd manager
		MyZDDNode oneZdd = ((MyZBDDClauseCollection) one_clauses).zdd;
		new_neg = zddManager.getSubset(oneZdd, neg_value);
		new_pos = zddManager.getSubset(oneZdd, pos_value);		
		
		long total_resolution = new_neg.count() * new_pos.count();
		int number_splits = num_splits;
		if(one_clauses.getIteration_number() == clauses.getIteration_number()) {
			number_splits /= 3; // Khanh: why constant 3???
			if (number_splits == 0) 
				number_splits = 1; // Khanh: added to prevent division by zero later 
		}
		else{
			//rebuild this.zdd and another zdd based on the new zdd manager
			MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
			old_neg = zddManager.getSubset(anotherZdd, neg_value);
			old_pos = zddManager.getSubset(anotherZdd, pos_value);
			
			total_resolution += new_pos.count() * old_neg.count() + new_neg.count() * old_pos.count();
		}
		zddManager.cleanup();
		
		
		long average_resolution = total_resolution / number_splits + 1;
//		System.out.println(average_resolution + "\t" + total_resolution);
		long unit_count = (long) Math.sqrt(average_resolution) + 1;
		
		List<MyZDDNode> splits_new_neg = divideZDD(new_neg, unit_count, number_splits);
		List<MyZDDNode> splits_new_pos = divideZDD(new_pos, unit_count, number_splits);
		
		int new_key = ((MyZBDDClauseCollection) one_clauses).getKey();
		for(MyZDDNode node_neg: splits_new_neg){
			AbstractClauseCollection neg = new MyZBDDClauseCollection(one_clauses.getIteration_number(), node_neg);
			for(MyZDDNode node_pos: splits_new_pos){
				AbstractClauseCollection pos = new MyZBDDClauseCollection(one_clauses.getIteration_number(), node_pos);
				list.add(new Tuple2(new_key++, new Tuple2(pos, neg)));
			}
		}
		
		if(one_clauses.getIteration_number() != clauses.getIteration_number()) {
			List<MyZDDNode> splits_old_neg = null, splits_old_pos = null;
			if(new_pos.count() <= 1){
				splits_old_neg = new ArrayList<MyZDDNode>();
				splits_old_neg.add(old_neg);
			}
			else{
				splits_old_neg = divideZDD(old_neg, unit_count, number_splits);
			}
			
			if(new_neg.count() <= 1){
				splits_old_pos = new ArrayList<MyZDDNode>();
				splits_old_pos.add(old_pos);
			}
			else{
				splits_old_pos = divideZDD(old_pos, unit_count, number_splits);
			}
			
			for(MyZDDNode node_neg_old: splits_old_neg){
				AbstractClauseCollection neg_old = new MyZBDDClauseCollection(clauses.getIteration_number(), node_neg_old);
				for(MyZDDNode node_pos_new: splits_new_pos){
					AbstractClauseCollection pos_new = new MyZBDDClauseCollection(one_clauses.getIteration_number(), node_pos_new);
					list.add(new Tuple2(new_key++, new Tuple2(pos_new, neg_old)));
				}
			}
			
			for(MyZDDNode node_neg_new: splits_new_neg){
				AbstractClauseCollection neg_new = new MyZBDDClauseCollection(one_clauses.getIteration_number(), node_neg_new);
				for(MyZDDNode node_pos_old: splits_old_pos){
					AbstractClauseCollection pos_old = new MyZBDDClauseCollection(clauses.getIteration_number(), node_pos_old);
					list.add(new Tuple2(new_key++, new Tuple2(pos_old, neg_new)));
				}
			}
		}
		
		long etime = System.currentTimeMillis();
		BigSAT.splittime += (etime - stime);
//		System.err.println(this.getKey() + "\tSplit time:\t" + (etime - stime));
		
//		one_clauses.clean();
//		clauses.clean();
		
		return list;
	}
	
//	private List<MyZDDNode> divideZDD(MyZDDNode zdd, int num, int number_splits){
//		long count = zdd.count() / num + 1;
//		return divideZDD(zdd, count, number_splits);
//	}

	private static List<MyZDDNode> divideZDD(MyZDDNode zdd, long unit_count, int number_splits) {
//		System.err.println(unit_count + "\t" + zdd.count());
		List<MyZDDNode> list = new ArrayList<MyZDDNode>();
		if(zdd.isZERO()){
			return list; 
		}
		
		if(unit_count * number_splits < zdd.count()){
			long u_count = zdd.count() / number_splits + 1;
			return divideZDD(zdd, u_count, number_splits);
		}
		
		if(zdd.count() <= 20){
			list.add(zdd);
			return list;
		}
		
		while(zdd.count() >= unit_count * 2){
			Tuple2<MyZDDNode, MyZDDNode> tuple = splitZDD(zdd, unit_count);
			list.add(tuple._1());
			zdd = tuple._2();
		}
//		if(zdd.count() > unit_count){
//			Tuple2<MyZDDNode, MyZDDNode> tuple = splitZDD(zdd, unit_count);
//			list.add(tuple._1);
//			list.add(tuple._2);
//		}
//		else{
//		}
		list.add(zdd);
		
		return list;
	}


	public static Tuple2<AbstractClauseCollection, AbstractClauseCollection> removeDuplicates(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses) {
		//initialize a new zdd manager
		MyZDDManager zddManager = new MyZDDManager();
		MyZDDNode oneZdd = ((MyZBDDClauseCollection) one_clauses).zdd;
		MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
		MyZDDNode oneRetZdd = zddManager.subsumedDiff(oneZdd, anotherZdd);
		MyZDDNode anotherRetZdd = zddManager.subsumedDiff(anotherZdd, oneRetZdd);
		zddManager.cleanup();
		
		AbstractClauseCollection one = new MyZBDDClauseCollection(one_clauses.getIteration_number(), oneRetZdd);
		AbstractClauseCollection another = new MyZBDDClauseCollection(clauses.getIteration_number(), anotherRetZdd);

//		one_clauses.clean();
//		clauses.clean();
		assert(!another.isEmpty() || !one.isEmpty());
		if(one.isEmpty()){
			return new Tuple2<AbstractClauseCollection, AbstractClauseCollection>(another, another);
		}
		
		if(another.isEmpty()){
			return new Tuple2<AbstractClauseCollection, AbstractClauseCollection>(one, one);
		}
		
		return new Tuple2<AbstractClauseCollection, AbstractClauseCollection>(one, another);
	}
	
	
	
//	@Override
//	public List<AbstractClauseCollection> doResolutionCollection(AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator, int iteration_num) {
//		assert(this.getKey() == clauses.getKey());
//		List<AbstractClauseCollection> list = new ArrayList<AbstractClauseCollection>();
//		
//		//initialize a new zdd manager
//		this.zddManager = new MyZDDManager();
//		
//		//rebuild this.zdd and another zdd based on the new zdd manager
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
////		MyZDDNode anotherZdd = this.zddManager.rebuildZBDD(((MyZBDDClauseCollection) clauses).zdd);
//		MyZDDNode oneZdd = zdd;
//		MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
//		
//		//remove the first literal before resolution
//		MyZDDNode zdd1 = oneZdd.getHigh();
//		MyZDDNode zdd2 = anotherZdd.getHigh();
//		//two unit clauses generating empty clause
//		if(zdd1.isONE() && zdd2.isONE()){
//			accumulator.add(1);
//			return list;
//		}
//		
//		//resolution
//		MyZDDNode retZdd = zddManager.productSTFree(zdd1, zdd2);
////		BigSAT.tcount += retZdd.count();
//
//		assert(!retZdd.isONE());
//		if(retZdd.isZERO() || retZdd.isONE()){
//			return list;
//		}
//		
//		//get a list of ZBDDs belonging to different buckets
//		do{
//			MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), zddManager.ZERO, retZdd.getHigh());
//			AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
//			list.add(cc);
//			retZdd = retZdd.getLow();
//		}
//		while(!retZdd.isZERO());
//		
//		return list;
//	}
	
	public static AbstractClauseCollection doResolutionCollection(AbstractClauseCollection one_clauses, AbstractClauseCollection clauses, Accumulator<Integer> accumulator) {
		assert(((MyZBDDClauseCollection) one_clauses).getKey() == clauses.getKey());
		
		//initialize a new zdd manager
		MyZDDManager zddManager = new MyZDDManager();
		
		//remove the first literal before resolution
		MyZDDNode zdd1 = ((MyZBDDClauseCollection) one_clauses).zdd.getHigh();
		MyZDDNode zdd2 = ((MyZBDDClauseCollection) clauses).zdd.getHigh();
		//two unit clauses generating empty clause
		if(zdd1.isONE() && zdd2.isONE()){
			accumulator.add(1);
			System.err.println(((MyZBDDClauseCollection) one_clauses).getKey());
			AbstractClauseCollection cc = new MyZBDDClauseCollection(-1, MyZDDManager.ZERO);
			
			return cc;
		}
		
		//resolution
		MyZDDNode retZdd = zddManager.productSTFree(zdd1, zdd2);
//		BigSAT.tcount += retZdd.count();
		zddManager.cleanup();

		assert(!retZdd.isONE());
		if(retZdd.isZERO() || retZdd.isONE()){
			AbstractClauseCollection cc = new MyZBDDClauseCollection(-1, MyZDDManager.ZERO);
			
			return cc;
		}
		
		return new MyZBDDClauseCollection(-1, retZdd);
	}
	
	
//	@Override
//	public List<Tuple2<Integer, AbstractClauseCollection>> doResolutionPair(AbstractClauseCollection clauses, Accumulator<Integer> accumulator, final int iteration_num) {
////		assert(this.getKey() == clauses.getKey());
//		List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
//		
//		//initialize a new zdd manager
//		this.zddManager = new MyZDDManager();
//		MyZDDNode retZdd = null;
//		
//		int pos_value = this.getKey() * 2, neg_value = pos_value - 1;
//		MyZDDNode new_neg = null, new_pos = null, old_neg = null, old_pos = null;
//
//		//rebuild this.zdd based on the new zdd manager
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
//		MyZDDNode oneZdd = zdd;
//		new_neg = this.zddManager.getSubset(oneZdd, neg_value);
//		new_pos = this.zddManager.getSubset(oneZdd, pos_value);
//		
//		if(this.getIteration_number() == clauses.getIteration_number()){
//			//do self join
//			retZdd = doPair(new_neg, new_pos, accumulator);
//		}
//		else{
//			//rebuild another zdd based on the new zdd manager
////			MyZDDNode anotherZdd = this.zddManager.rebuildZBDD(((MyZBDDClauseCollection) clauses).zdd);
//			MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
//			old_neg = this.zddManager.getSubset(anotherZdd, neg_value);
//			old_pos = this.zddManager.getSubset(anotherZdd, pos_value);
//			
//			//do self join
//			MyZDDNode selfRetNode = doPair(new_neg, new_pos, accumulator);
//			
//			//do +new with -old
//			MyZDDNode newposRetNode = doPair(new_pos, old_neg, accumulator);
//			
//			//do -new with +old
//			MyZDDNode newnegRetNode = doPair(new_neg, old_pos, accumulator);
//			
//			retZdd = zddManager.unionSubsumFree(zddManager.unionSubsumFree(selfRetNode, newposRetNode), newnegRetNode);
//		}
//		
////		BigSAT.tcount += retZdd.count();
//		
//		assert(!retZdd.isONE());
//		if(retZdd.isZERO() || retZdd.isONE()){
//			return list;
//		}
//		
//		//get a list of ZBDDs belonging to different buckets
//		do{
////			if(MyZDDManager.isNegConsecutive(retZdd)){
////				MyZDDNode newlowzdd = zddManager.mk(retZdd.getLow().getValue(), zddManager.ZERO, retZdd.getLow().getHigh());
////				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), newlowzdd, retZdd.getHigh());
////				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, null, newzdd);
////				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
////				retZdd = retZdd.getLow().getLow();
////			}
////			else{
////				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), zddManager.ZERO, retZdd.getHigh());
////				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, null, newzdd);
////				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
////				retZdd = retZdd.getLow();
////			}
//			
//			MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), zddManager.ZERO, retZdd.getHigh());
//			AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
//			list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
//			retZdd = retZdd.getLow();
//		}
//		while(!retZdd.isZERO());		
//		
//		return list;
//	}

	private static MyZDDNode doPair(MyZDDNode zdd1, MyZDDNode zdd2, Accumulator<Integer> accumulator) {
		//two unit clauses generating empty clause
		if(zdd1.isZERO() || zdd2.isZERO()){
			return MyZDDManager.ZERO;
		}
		
		if(zdd1.isONE() && zdd2.isONE()){
			accumulator.add(1);
			return MyZDDManager.ZERO;
		}
		
		MyZDDManager zddManager = new MyZDDManager();
		//resolution
		MyZDDNode retZdd = zddManager.productSTFree(zdd1, zdd2);
		zddManager.cleanup();

		assert(!retZdd.isONE());
		if(retZdd.isZERO() || retZdd.isONE()){
			return MyZDDManager.ZERO;
		}
		
		return retZdd;
	}

	
//	@Override
//	public AbstractClauseCollection combinePair(AbstractClauseCollection clauses) {
//		// TODO Auto-generated method stub
//		return clauses.addClauses(this);
//	}
	

//	@Override
//	public long countResolutions(AbstractClauseCollection clauses) {
//		// TODO Auto-generated method stub
//		this.zddManager = new MyZDDManager();
//		int pos_value = this.getKey() * 2, neg_value = pos_value - 1;
//		MyZDDNode new_neg = null, new_pos = null, old_neg = null, old_pos = null;
//		
//		//get count of new
//		if (this.isNegative()) {
//			new_neg = this.zdd.getHigh();
//			
//			if (!this.zdd.getLow().isZERO()) {
//				assert (this.zdd.getLow().getValue() == pos_value);
//				new_pos = this.zdd.getLow().getHigh();
//			} 
//			else {
//				new_pos = this.zddManager.ZERO;
//			}
//		} 
//		else if (this.isPositive()) {
//			assert (this.zdd.getLow().isZERO());
//			new_pos = this.zdd.getHigh();
//			new_neg = this.zddManager.ZERO;
//		}
//		
//		if(this.getIteration_number() == clauses.getIteration_number()){
//			return new_neg.count() * new_pos.count();
//		}
//		else{
//			MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
//			if (((MyZBDDClauseCollection) clauses).isNegative()) {
//				old_neg = anotherZdd.getHigh();
//
//				if (!anotherZdd.getLow().isZERO()) {
//					assert (anotherZdd.getLow().getValue() == pos_value);
//					old_pos = anotherZdd.getLow().getHigh();
//				} 
//				else {
//					old_pos = this.zddManager.ZERO;
//				}
//			} 
//			else if (((MyZBDDClauseCollection) clauses).isPositive()) {
//				assert (anotherZdd.getLow().isZERO());
//				old_pos = anotherZdd.getHigh();
//				old_neg = this.zddManager.ZERO;
//			}
//			
//			long new_neg_count = new_neg.count(), new_pos_count = new_pos.count(), old_neg_count = old_neg.count(), old_pos_count = old_pos.count();
//			return new_neg_count * new_pos_count + new_neg_count * old_pos_count + new_pos_count * old_neg_count;
//		}
//		
//	}

//	@Override
//	public List<Tuple2<Integer, AbstractClauseCollection>> renamePartialCollection(int number_buckets, long cut_count) {
//		// TODO Auto-generated method stub
//		List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
//		
//		this.zddManager = new MyZDDManager();
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
//		MyZDDNode oneZdd = zdd;
//		
//		MyZDDNode retZdd = renamePortion(oneZdd.getValue(), oneZdd.getHigh(), cut_count, number_buckets);
//		
//		//get a list of ZBDDs belonging to different buckets
//		do{
//			if(MyZDDManager.isNegConsecutive(retZdd)){
//				MyZDDNode newlowzdd = zddManager.mk(retZdd.getLow().getValue(), zddManager.ZERO, retZdd.getLow().getHigh());
//				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), newlowzdd, retZdd.getHigh());
//				AbstractClauseCollection cc = new MyZBDDClauseCollection(this.getIteration_number(), newzdd);
//				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
//				retZdd = retZdd.getLow().getLow();
//			}
//			else{
//				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), zddManager.ZERO, retZdd.getHigh());
//				AbstractClauseCollection cc = new MyZBDDClauseCollection(this.getIteration_number(), newzdd);
//				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
//				retZdd = retZdd.getLow();
//			}
//			
//		}
//		while(!retZdd.isZERO());
//		
//		return list;
//	}

//	private MyZDDNode renamePortion(int value, MyZDDNode zdd, long cut_count, int number_buckets) {
//		// TODO Auto-generated method stub
//		Tuple2<MyZDDNode, MyZDDNode> tuple = splitZDD(zdd, cut_count);
//		MyZDDNode old = this.zddManager.mk(value, this.zddManager.ZERO, tuple._2());
//		MyZDDNode tmp = this.zddManager.mk(value + number_buckets * 2, MyZDDManager.ZERO, MyZDDManager.ONE);
//		MyZDDNode renamed = this.zddManager.productSTFree(tuple._1(), tmp);
//		return this.zddManager.unionSubsumFree(old, renamed);
//	}
	
	
//	/**split zdd into two parts based on the given percentage, where the first zdd in the pair will be renamed
//	 * @param zdd
//	 * @param percentage
//	 * @return
//	 */
//	private Tuple2<MyZDDNode, MyZDDNode> splitZDD(MyZDDNode zdd, double percentage){
//		long cut_count = (long) (zdd.count() * percentage);
//		return splitZDD(zdd, cut_count);
//	}
	
	private static Tuple2<MyZDDNode, MyZDDNode> splitZDD(MyZDDNode zdd, long cut_count){
		MyZDDManager zddManager = new MyZDDManager();
		Tuple2<MyZDDNode, MyZDDNode> r = zddManager.splitZDD(zdd, cut_count);
		zddManager.cleanup();
		return r;
	}

	
//	@Override
//	public List<Tuple2<Integer, AbstractClauseCollection>> doResolutionPair_balance(AbstractClauseCollection clauses,
//			Accumulator<Integer> accumulator, int iteration_num, double percentage, int number_buckets) {
//		List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
//
//		this.zddManager = new MyZDDManager();
//		MyZDDNode retZdd = MyZDDManager.ZERO;
//		
//		int pos_value = this.getKey() * 2, neg_value = pos_value - 1;
//		MyZDDNode new_neg = null, new_pos = null, old_neg = null, old_pos = null;
//		
//		//rebuild this.zdd based on the new zdd manager
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
//		MyZDDNode oneZdd = zdd;
//		new_neg = this.zddManager.getSubset(oneZdd, neg_value);
//		new_pos = this.zddManager.getSubset(oneZdd, pos_value);
//		
//		Tuple2<MyZDDNode, MyZDDNode> tuple_neg = splitZDD(new_neg, percentage);
//		Tuple2<MyZDDNode, MyZDDNode> tuple_pos = splitZDD(new_pos, percentage); 
//
//		//generate renamed negative clauses
//		MyZDDNode tmp_neg = this.zddManager.mk(neg_value + number_buckets * 2, MyZDDManager.ZERO, MyZDDManager.ONE);
//		MyZDDNode rename_neg = tuple_neg._1();
//		MyZDDNode renamed_neg = this.zddManager.productSTFree(rename_neg, tmp_neg);
//		retZdd = this.zddManager.unionSubsumFree(retZdd, renamed_neg);
//		//generate renamed positive clauses
//		MyZDDNode tmp_pos = this.zddManager.mk(pos_value + number_buckets * 2, MyZDDManager.ZERO, MyZDDManager.ONE);
//		MyZDDNode rename_pos = tuple_pos._1();
//		MyZDDNode renamed_pos = this.zddManager.productSTFree(rename_pos, tmp_pos);
//		retZdd = this.zddManager.unionSubsumFree(retZdd, renamed_pos);
//		
//		MyZDDNode remain_neg = new_neg;
//		if(!rename_neg.isZERO()){
//			remain_neg = this.zddManager.unionSubsumFree(tuple_neg._2(), tmp_pos);
//		}
//		MyZDDNode remain_pos = new_pos;
//		if(!rename_pos.isZERO()){
//			remain_pos = this.zddManager.unionSubsumFree(tuple_pos._2(), tmp_neg);
//		}
//		
//		
//		if(this.getIteration_number() == clauses.getIteration_number()){
//			//do self join
//			retZdd = this.zddManager.unionSubsumFree(retZdd, doPair(remain_neg, remain_pos, accumulator));
//		}
//		else{
//			//rebuild this.zdd and another zdd based on the new zdd manager
////			MyZDDNode anotherZdd = this.zddManager.rebuildZBDD(((MyZBDDClauseCollection) clauses).zdd);
//			MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
//			old_neg = this.zddManager.getSubset(anotherZdd, neg_value);
//			old_pos = this.zddManager.getSubset(anotherZdd, pos_value);
//			
//			
//			Tuple2<MyZDDNode, MyZDDNode> tuple_neg_old = splitZDD(old_neg, percentage);
//			Tuple2<MyZDDNode, MyZDDNode> tuple_pos_old = splitZDD(old_pos, percentage);
//			
//			//generate renamed negative clauses
//			MyZDDNode rename_neg_old = tuple_neg_old._1();
//			MyZDDNode renamed_neg_old = this.zddManager.productSTFree(rename_neg_old, tmp_neg);
//			retZdd = this.zddManager.unionSubsumFree(retZdd, renamed_neg_old);
//			//generate renamed positive clauses
//			MyZDDNode rename_pos_old = tuple_pos_old._1();
//			MyZDDNode renamed_pos_old = this.zddManager.productSTFree(rename_pos_old, tmp_pos);
//			retZdd = this.zddManager.unionSubsumFree(retZdd, renamed_pos_old);
//			
//			MyZDDNode remain_neg_old = old_neg;
//			if(!rename_neg_old.isZERO()){
//				remain_neg_old = tuple_neg_old._2();
//				if(rename_neg.isZERO()){
//					remain_neg_old = this.zddManager.unionSubsumFree(remain_neg_old, tmp_pos);
//				}
//			}
//			MyZDDNode remain_pos_old = old_pos;
//			if(!rename_pos_old.isZERO()){
//				remain_pos_old = tuple_pos_old._2();
//				if(rename_pos.isZERO()){
//					remain_pos_old = this.zddManager.unionSubsumFree(remain_pos_old, tmp_neg);
//				}
//			}
//			
//			
//			//do self join
//			MyZDDNode selfRetNode = doPair(remain_neg, remain_pos, accumulator);
//			
//			//do +new with -old
//			MyZDDNode newposRetNode = doPair(remain_pos, remain_neg_old, accumulator);
//			
//			//do -new with +old
//			MyZDDNode newnegRetNode = doPair(remain_neg, remain_pos_old, accumulator);
//			
//			retZdd = this.zddManager.unionSubsumFree(retZdd, zddManager.unionSubsumFree(zddManager.unionSubsumFree(selfRetNode, newposRetNode), newnegRetNode));
//		}
//		
//		assert(!retZdd.isONE());
//		if(retZdd.isZERO() || retZdd.isONE()){
//			return list;
//		}
//		
//		//get a list of ZBDDs belonging to different buckets
//		do{
////			if(MyZDDManager.isNegConsecutive(retZdd)){
////				MyZDDNode newlowzdd = zddManager.mk(retZdd.getLow().getValue(), zddManager.ZERO, retZdd.getLow().getHigh());
////				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), newlowzdd, retZdd.getHigh());
////				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, null, newzdd);
////				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
////				retZdd = retZdd.getLow().getLow();
////			}
////			else{
////				MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), zddManager.ZERO, retZdd.getHigh());
////				AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, null, newzdd);
////				list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
////				retZdd = retZdd.getLow();
////			}
//			
//			MyZDDNode newzdd = zddManager.mk(retZdd.getValue(), MyZDDManager.ZERO, retZdd.getHigh());
//			AbstractClauseCollection cc = new MyZBDDClauseCollection(iteration_num, newzdd);
//			list.add(new Tuple2<Integer, AbstractClauseCollection>(cc.getKey(), cc));
//			retZdd = retZdd.getLow();
//		}
//		while(!retZdd.isZERO());		
//		
//		return list;
//	}

//	@Override
//	public AbstractClauseCollection renameAndcombinePair(AbstractClauseCollection clauses, double percentage, int number_buckets) {
//		// TODO Auto-generated method stub
//		this.zddManager = new MyZDDManager();
//		
//		int pos_value = this.getKey() * 2, neg_value = pos_value - 1;
//		MyZDDNode new_neg = null, new_pos = null, old_neg = null, old_pos = null;
//		
//		//rebuild this.zdd based on the new zdd manager
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
//		MyZDDNode oneZdd = zdd;
//		new_neg = this.zddManager.getSubset(oneZdd, neg_value);
//		new_pos = this.zddManager.getSubset(oneZdd, pos_value);
//		
//		Tuple2<MyZDDNode, MyZDDNode> tuple_neg = splitZDD(new_neg, percentage);
//		Tuple2<MyZDDNode, MyZDDNode> tuple_pos = splitZDD(new_pos, percentage); 		
//				
//		MyZDDNode tmp_neg = this.zddManager.mk(neg_value + number_buckets * 2, MyZDDManager.ZERO, MyZDDManager.ONE);
//		MyZDDNode tmp_pos = this.zddManager.mk(pos_value + number_buckets * 2, MyZDDManager.ZERO, MyZDDManager.ONE);
//		
//		MyZDDNode remain_neg = new_neg;
//		if(!tuple_neg._1().isZERO()){
//			remain_neg = this.zddManager.unionSubsumFree(tuple_neg._2(), tmp_pos);
//		}
//		MyZDDNode remain_pos = new_pos;
//		if(!tuple_pos._1().isZERO()){
//			remain_pos = this.zddManager.unionSubsumFree(tuple_pos._2(), tmp_neg);
//		}
//		
//		MyZDDNode neg_o = this.zddManager.mk(neg_value, MyZDDManager.ZERO, remain_neg);
//		MyZDDNode pos_o = this.zddManager.mk(pos_value, MyZDDManager.ZERO, remain_pos);
//		
//		MyZDDNode old = null;
//		if(this.getIteration_number() == clauses.getIteration_number()){
//			old = this.zddManager.unionSubsumFree(neg_o, pos_o);
//		}
//		else{
//			//rebuild this.zdd and another zdd based on the new zdd manager
////			MyZDDNode anotherZdd = this.zddManager.rebuildZBDD(((MyZBDDClauseCollection) clauses).zdd);
//			MyZDDNode anotherZdd = ((MyZBDDClauseCollection) clauses).zdd;
//			old_neg = this.zddManager.getSubset(anotherZdd, neg_value);
//			old_pos = this.zddManager.getSubset(anotherZdd, pos_value);
//			
//			
//			Tuple2<MyZDDNode, MyZDDNode> tuple_neg_old = splitZDD(old_neg, percentage);
//			Tuple2<MyZDDNode, MyZDDNode> tuple_pos_old = splitZDD(old_pos, percentage);
//			
//			MyZDDNode remain_neg_old = old_neg;
//			if(!tuple_neg_old._1().isZERO()){
//				remain_neg_old = tuple_neg_old._2();
//				if(tuple_neg._1().isZERO()){
//					remain_neg_old = this.zddManager.unionSubsumFree(remain_neg_old, tmp_pos);
//				}
//			}
//			MyZDDNode remain_pos_old = old_pos;
//			if(!tuple_pos_old._1().isZERO()){
//				remain_pos_old = tuple_pos_old._2();
//				if(tuple_pos._1().isZERO()){
//					remain_pos_old = this.zddManager.unionSubsumFree(remain_pos_old, tmp_neg);
//				}
//			}
//			
//			MyZDDNode neg_oo = this.zddManager.mk(neg_value, MyZDDManager.ZERO, remain_neg_old);
//			MyZDDNode pos_oo = this.zddManager.mk(pos_value, MyZDDManager.ZERO, remain_pos_old);
//
//			old = this.zddManager.unionSubsumFree(this.zddManager.unionSubsumFree(neg_o, pos_o), this.zddManager.unionSubsumFree(neg_oo, pos_oo));
//		}
//		
//		return new MyZBDDClauseCollection(clauses.getIteration_number(), old);
//	}

	

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (this.zdd.isONE() || this.zdd.isZERO());
	}
	
	@Override
	public boolean isPositive() {
		// TODO Auto-generated method stub
		return zdd.getValue() % 2 == 0;
	}

	@Override
	public boolean isNegative() {
		// TODO Auto-generated method stub
		return zdd.getValue() % 2 != 0;
	}

	@Override
	public Integer getKey() {
		// TODO Auto-generated method stub
		int var = zdd.getValue();
		assert(var != -1 && var != 0);
		return var % 2 == 0 ? var / 2 : (var + 1) / 2;
	}
	
	public MyZDDNode getZdd() {
		return zdd;
	}

	@Override
	public long getNumClauses() {
		// TODO Auto-generated method stub
		return this.zdd.count();
	}
	

//	public String toString(){
//		this.zddManager = new MyZDDManager();
////		MyZDDNode oneZdd = this.zddManager.rebuildZBDD(zdd);
//		MyZDDNode oneZdd = zdd;
//		return MyZDDPrinter.getPaths(oneZdd, zddManager).toString() + ":" + this.getIteration_number();
//	}
	
//	public void printZBDDs(){
//		System.out.println(MyZDDPrinter.getPaths(zdd, zddManager));
//	}
	
	
	public static void main(String[] args) {
//		int[] a1 = {1, 2, -8};
//		int[] a2 = {1, 2, -5};
//		MyZBDDClauseCollection zdd1 = new MyZBDDClauseCollection(1);
//		zdd1.addClause(new BucketClause(a1));
//		zdd1.addClause(new BucketClause(a2));
//		zdd1.printZBDDs();
//		
//		int[] a3 = {-1, -2};
//		int[] a4 = {-1, 5, 8};
//		MyZBDDClauseCollection zdd2 = new MyZBDDClauseCollection(1);
//		zdd2.addClause(new BucketClause(a3));
//		zdd2.addClause(new BucketClause(a4));
//		zdd2.printZBDDs();
//		
//		List<AbstractClauseCollection> list = zdd1.doResolutionCollection(zdd2, null, 1);
//		for(AbstractClauseCollection cc: list){
//			((MyZBDDClauseCollection) cc).printZBDDs();
//		}
//		
//		System.out.println(1 % 2);
	}
	
}
