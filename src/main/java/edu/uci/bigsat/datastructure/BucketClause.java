package edu.uci.bigsat.datastructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.uci.bigsat.util.QuickSort;

public class BucketClause implements Serializable {

	public static final String Delimiter = " ";

	//list of literals
	protected List<Integer> literals;
	
	//0:initial		1:true		-1:false		2:positive		-2:negative
	protected byte indicator = 0;
	

	public BucketClause(){}

	
	public BucketClause(int[] tmp) {
		QuickSort.sort(tmp);
		this.literals = new ArrayList<Integer>(tmp.length);
		for(int l: tmp){
			this.literals.add(l);
		}
		this.indicator = (byte) ((this.literals.get(0) > 0) ? 2 : -2);
	}
	
	
	public BucketClause(String line) {
		literals = new ArrayList<Integer>();
		this.indicator = initializeLiterals(this.literals, line);
//		System.out.println(line);
		if(this.isInitial()){
			this.indicator = (byte) ((this.literals.get(0) > 0) ? 2 : -2);
		}

	}

	public BucketClause(BucketClause firstC, BucketClause secondC){
		literals = new ArrayList<Integer>();
		
		this.indicator = createLiterals(firstC, secondC, this.literals);
	    
		if(this.isInitial()){
			this.indicator = (byte) ((this.literals.get(0) > 0) ? 2 : -2);
		}
	}


	public BucketClause(List<Integer> alist) {
		// TODO Auto-generated constructor stub
		this.literals = alist;
		this.indicator = (byte) ((this.literals.get(0) > 0) ? 2 : -2);
	}
	

	public static byte initializeLiterals(List<Integer> literals, String line) {
		String[] literals_string = line.split(Delimiter);
		//each clause ends with "0"
		assert(literals_string[literals_string.length - 1].equals("0"));
		
		int[] tmp = new int[literals_string.length - 1];
		for(int i = 0; i < literals_string.length - 1; i++){
			int literal = Integer.parseInt(literals_string[i]);
			assert(literal != 0);
			tmp[i] = literal;
		}
		
		QuickSort.sort(tmp);
		
		//post-processing to remove duplicates and true clause
		assert(tmp.length != 0);
		literals.add(tmp[0]);
		for(int i = 1; i < tmp.length; i++){
			int literal = tmp[i];
			int topLiteral = literals.get(literals.size() - 1);
			if(literal != topLiteral){
				literals.add(literal);
				if (literal + topLiteral == 0){
					return 1;
				}
			}
		}
		return 0;
	}


	public static byte createLiterals(BucketClause firstC, BucketClause secondC, List<Integer> literals) {
		List<Integer> firstList = firstC.getLiterals();
		List<Integer> secondList = secondC.getLiterals();
		
		int i = 1, j = 1;
	    while (i < firstList.size() && j < secondList.size())
	    {
	    	int c = compare(firstList.get(i), secondList.get(j));
	        switch (c){
	        case -1:
	        	literals.add(firstList.get(i));
	        	i++;
	        	break;
	        case 1:
	        	literals.add(secondList.get(j));
	        	j++;
	        	break;
	        case 2:
	        	literals.add(firstList.get(i));
	        	i++;
	        	j++;
	        	break;
	        case 0:
	        	literals.add(firstList.get(i));
	        	literals.add(secondList.get(j));
	        	return 1;
//	        	return;
	        default:
	        	throw new RuntimeException("Wrong comparison result!!!");
	        }
	        		
	    }

	    while (i < firstList.size())
	    {
	    	literals.add(firstList.get(i));
	        i++;
	    }

	    while (j < secondList.size())
	    {
	    	literals.add(secondList.get(j));
	        j++;
	    }
	    
	    
	    //if it's an empty clause
		if(literals.isEmpty()){
			return -1;
		}
	    return 0;
	}
	
	private static int compare(int i, int j){
		if(i == j){
			return 2;
		}
		int ai = Math.abs(i), aj = Math.abs(j);
		if(ai > aj){
			return 1;
		}
		else if(ai < aj){
			return -1;
		}
		else {
			return 0;
		}
	}

	public int hashCode(){
		int result = 7;
		result = 31 * result + this.indicator;
		result = 31 * result + this.literals.size();
		return result;
	}
	
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		
		BucketClause c = (BucketClause) obj;
		if(c.indicator == this.indicator && c.literals.size() == this.literals.size()){
			for(int literal: this.literals){
				if(!c.literals.contains(literal)){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	
	public String toString(){
		StringBuilder builder = new StringBuilder();
		builder.append(this.literals.toString()).append(",").append(this.getKey()).append(",").append(this.indicator);
		return builder.toString();
	}
	
	
//	public Integer generateKey(Broadcast<Integer[]> ordering) {
//		for (Integer var : ordering.value()) {
//			if (this.literals.contains(var)) {
//				this.indicator = 2;
//				return var;
//			}
//			
//			if(this.literals.contains(0-var)){
//				this.indicator = -2;
//				return var;
//			}
//		}
//
//		return null;
//	}
	
	public boolean isUnitClause(){
		return this.literals.size() == 1;
	}
	
	
	public boolean isPositive(){
		return this.indicator == 2;
	}
	
	public boolean isNegative(){
		return this.indicator == -2;
	}
	
	
	public boolean isTrue() {
		// TODO Auto-generated method stub
		return this.indicator == 1;
	}
	
	public boolean isFalse(){
		return this.indicator == -1;
	}
	
	private boolean isInitial() {
		// TODO Auto-generated method stub
		return this.indicator == 0;
	}

	public List<Integer> getLiterals() {
		return literals;
	}

	public int getKey() {
		if(this.isTrue() || this.isFalse()){
			return 0;
		}
		int k = this.literals.get(0);
//		return k > 0 ? k : 0 - k;
		return Math.abs(k);
	}
	
	
	public static void main(String[] args) {
		BucketClause c = new BucketClause("1 3 -2 1 -5 -6 0");
		BucketClause c1 = new BucketClause("1 -2 6 -5 0");
//		Clause c = new Clause("1 3 -2 1 -5 0");
		System.out.println(c.toString());
		System.out.println(c1);
		System.out.println(new BucketClause(c, c1));
	}
	
	

//	static class LiteralComparator implements Comparator<Integer>{
//
//		@Override
//		public int compare(Integer o1, Integer o2) {
//			// TODO Auto-generated method stub
//			int ai = Math.abs(o1), aj = Math.abs(o2);
//			if(ai > aj){
//				return 1;
//			}
//			else if(ai < aj){
//				return -1;
//			}
//			else{
//				return o1.compareTo(o2);
//			}
//		}
//		
//	}
}
