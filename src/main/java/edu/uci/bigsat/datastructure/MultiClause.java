package edu.uci.bigsat.datastructure;

import java.util.List;

public class MultiClause extends BucketClause{
	
	private final int key;

	public MultiClause(List<Integer> list, int key, byte indicator) {
		super();
		this.key = key;
		this.indicator = indicator;
		this.literals = list;
	}
	
	
	public int hashCode(){
		int r = super.hashCode();
		r = r * 31 + this.key;
		return r;
	}
	
	public boolean equals(Object obj){
//		boolean superequal = super.equals(obj);
//		if(superequal){
//			return this.key == ((Clause) obj).key;
//		}
//		return false;
		
		if(this == obj){
			return true;
		}
		
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		
		MultiClause c = (MultiClause) obj;
		if(c.key == this.key && c.indicator == this.indicator && c.literals.size() == this.literals.size()){
			for(int literal: this.literals){
				if(!c.literals.contains(literal)){
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	
	public int getKey(){
		return this.key;
	}

	
	
	

}
