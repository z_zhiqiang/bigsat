package edu.uci.bigsat.preprocessor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;

import edu.uci.bigsat.datastructure.BucketClause;
import scala.Tuple2;

public class Preprocessor {
	
	public static JavaRDD<int[]> preprocess(final int renaming, JavaSparkContext jsc, final String input, final int number_buckets, final int number_partitions) {
		//initiate an accumulator for invalid input
		final Accumulator<Integer> invalidInputAccumulator = jsc.intAccumulator(0); 
		
		//check the cnf input whether its literals are valid (within the valid name range) and at the same time, remove duplicates and tautological clauses  
		JavaRDD<Set<Integer>> checkedCNF = checkInputValidation(jsc, input, number_partitions, number_buckets, invalidInputAccumulator).cache();
		System.out.println("Number of clauses:\t" + checkedCNF.count());
		if(invalidInputAccumulator.value() != 0){
			System.err.println("Application stopped due to the invalid literal!!!");
			jsc.cancelAllJobs();
			jsc.stop();
			System.exit(0);
		}
		System.out.println(checkedCNF.collect());
		
		//rename literals
		JavaRDD<int[]> renamedCNF = renameCNF(renaming == 0 ? false : true, checkedCNF, jsc.broadcast(getOrdering(checkedCNF)));
		//for debugging
		for(int[] array: renamedCNF.collect()){
			printIntArray(array);
		}
		
		return renamedCNF;
	}
	
	
	/**check the cnf input whether its literals are valid (within the valid name range), 
	 * and at the same time, remove duplicated literals and tautological clauses   
	 * @param jsc
	 * @param input
	 * @param number_partitions
	 * @param number_buckets
	 * @param invalidInputAccumulator
	 * @return
	 */
	private static JavaRDD<Set<Integer>> checkInputValidation(final JavaSparkContext jsc, String input, int number_partitions, final int number_buckets, final Accumulator<Integer> invalidInputAccumulator) {
		//create the initial cnf rdd
		JavaRDD<String> cnf_raw = jsc.textFile(input, number_partitions);
		//filter out comment lines
		JavaRDD<String> cnf_string = cnf_raw.filter(new Function<String, Boolean>(){

			public Boolean call(String arg0) throws Exception {
				// TODO Auto-generated method stub
				return !(arg0.startsWith("c") || arg0.startsWith("p"));
			}
			
		});
		System.out.println(cnf_string.collect());
		
		//map each line to a clause while checking validity and removing duplicated literals, then filter out tautological clauses
		JavaRDD<Set<Integer>> checkedCNF = cnf_string.map(new Function<String, Set<Integer>>() {

			@Override
			public Set<Integer> call(String line) throws Exception {
				// TODO Auto-generated method stub
				Set<Integer> set = new HashSet<Integer>();
				String[] literals_string = line.split(BucketClause.Delimiter);
				// each line ends with "0"
				assert (literals_string[literals_string.length - 1].equals("0"));

				for (int i = 0; i < literals_string.length - 1; i++) {
					int literal = Integer.parseInt(literals_string[i]);

					if (isInvalid(literal, number_buckets)) {
						invalidInputAccumulator.add(1);
						System.err.println("Invalid literal:\t" + literal);
						return null;
					}

					if (set.contains(0 - literal)) {
						return null;
					}
					set.add(literal);
				}
				return set;
			}

			private boolean isInvalid(int literal, int number_buckets) {
				// TODO Auto-generated method stub
				int al = Math.abs(literal);
				return al > number_buckets || al == 0;

			}
			
		}).filter(new Function<Set<Integer>, Boolean>() {

			@Override
			public Boolean call(Set<Integer> v1) throws Exception {
				// TODO Auto-generated method stub
				return v1 != null;
			}

		});
		
		return checkedCNF;
	}
	
	
	/**rename literals of the input cnf formula
	 * @param rename_flag
	 * @param checkedCNF
	 * @param renaming_map
	 * @return
	 */
	private static JavaRDD<int[]> renameCNF(final boolean rename_flag, JavaRDD<Set<Integer>> checkedCNF, final Broadcast<int[]> renaming_map) {
		JavaRDD<int[]> renamedCNF = checkedCNF.map(new Function<Set<Integer>, int[]>(){

			@Override
			public int[] call(Set<Integer> v1) throws Exception {
				// TODO Auto-generated method stub
				int[] array = new int[v1.size()];
				int index = 0;
				for(int old: v1){
					if(rename_flag){
						int aold = Math.abs(old);
						int anew = renaming_map.getValue()[aold - 1];
						array[index++] = old > 0 ? anew : 0 - anew;
					}
					else{
						array[index++] = old;
					}
				}
				return array;
			}});
		
		return renamedCNF;
	}
	
	
	/**get an array of ranked variables, which will be used for renaming variables: given an old variable "old", the new variable should be "renaming_map[old - 1]"
	 * @param checkedCNF
	 * @return
	 */
	private static int[] getOrdering(JavaRDD<Set<Integer>> checkedCNF) {
		// TODO Auto-generated method stub
		JavaPairRDD<Integer, Tuple2<Integer, Integer>> countsLiteral = checkedCNF.flatMapToPair(new PairFlatMapFunction<Set<Integer>, Integer, Tuple2<Integer, Integer>>(){

			@Override
			public Iterable<Tuple2<Integer, Tuple2<Integer, Integer>>> call(Set<Integer> t) throws Exception {
				// TODO Auto-generated method stub
				List<Tuple2<Integer, Tuple2<Integer, Integer>>> list = new ArrayList<Tuple2<Integer, Tuple2<Integer, Integer>>>();
				Tuple2<Integer, Integer> tuple;
				for(int literal: t){
					if(literal > 0){
						tuple = new Tuple2<Integer, Integer>(1, 0);
					}
					else{
						tuple = new Tuple2<Integer, Integer>(0, 1);
					}
					list.add(new Tuple2<Integer, Tuple2<Integer, Integer>>(Math.abs(literal), tuple));
				}
				return list;
			}}).reduceByKey(new Function2<Tuple2<Integer, Integer>, Tuple2<Integer, Integer>, Tuple2<Integer, Integer>>(){

				@Override
				public Tuple2<Integer, Integer> call(Tuple2<Integer, Integer> v1, Tuple2<Integer, Integer> v2) throws Exception {
					// TODO Auto-generated method stub
					return new Tuple2<Integer, Integer>(v1._1() + v2._1(), v1._2() + v2._2());
				}});
		
		JavaPairRDD<Integer, Integer> sortedVariables = countsLiteral.mapToPair(new PairFunction<Tuple2<Integer, Tuple2<Integer, Integer>>, Integer, Integer>(){

			@Override
			public Tuple2<Integer, Integer> call(Tuple2<Integer, Tuple2<Integer, Integer>> t) throws Exception {
				// TODO Auto-generated method stub
				return new Tuple2<Integer, Integer>(getScore(t._2()), t._1());
			}

			private int getScore(Tuple2<Integer, Integer> _2) {
				// TODO Auto-generated method stub
				if(_2._1() * _2._2() == 0){
					return Integer.MIN_VALUE;
				}
				return _2._1() * _2._2() - _2._1() - _2._2();
			}}).sortByKey(true);
		System.out.println(sortedVariables.collect());
		
		JavaRDD<Integer> rankedVariables = sortedVariables.map(new Function<Tuple2<Integer, Integer>, Integer>(){

			@Override
			public Integer call(Tuple2<Integer, Integer> v1) throws Exception {
				// TODO Auto-generated method stub
				return v1._2();
			}});
		System.out.println(rankedVariables.collect());
		
		List<Integer> rankedVariablesList = rankedVariables.collect(); 
		int[] renaming_map = new int[rankedVariablesList.size()];
		for(int i = 0; i < rankedVariablesList.size(); i++){
			int var = rankedVariablesList.get(i);
			renaming_map[var - 1] = i + 1;
		}
		
		//for debugging: print out renaming array
		printIntArray(renaming_map);
		
		return renaming_map;
	}
	
	private static void printIntArray(int[] renaming_map) {
		for(int i = 0; i < renaming_map.length; i++){
			System.out.println(i + ":" + renaming_map[i] + " ");
		}
		System.out.println();
	}

}
