package edu.uci.bigsat.solver;

import java.io.Serializable;
import java.util.Iterator;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;

import edu.uci.bigsat.datastructure.AbstractClauseCollection;
import edu.uci.bigsat.datastructure.BucketClause;
import edu.uci.bigsat.datastructure.MyZBDDClauseCollection;
import edu.uci.bigsat.datastructure.SetClauseCollection;
import edu.uci.bigsat.datastructure.TrieClauseCollection;
import edu.uci.bigsat.util.MyPartitioner;
import scala.Tuple2;

public abstract class AbstractSolver implements Serializable {
	
	//do resolution
	public abstract void doResolution(int datastructure_mode, JavaSparkContext jsc, final int number_buckets, JavaRDD<int[]> inputCNF, final int number_partitions, final int number_splits);
	
	
	protected static JavaPairRDD<Integer, Long> getStatisticsData(JavaPairRDD<Integer, AbstractClauseCollection> rdd) {
		JavaPairRDD<Integer, Long> countsRdd = rdd.mapValues(new Function<AbstractClauseCollection, Long>(){

			@Override
			public Long call(AbstractClauseCollection v1) throws Exception {
				// TODO Auto-generated method stub
				return v1.getNumClauses();
			}}).reduceByKey(new Function2<Long, Long, Long>(){

				@Override
				public Long call(Long v1, Long v2) throws Exception {
					// TODO Auto-generated method stub
					return v1 + v2;
				}}).sortByKey();
		return countsRdd;
	}
	
	
//	protected static JavaPairRDD<Integer, AbstractClauseCollection> initializePairedClauseCollections(JavaRDD<int[]> inputCNF, final int i) {
//		// TODO Auto-generated method stub
//		JavaPairRDD<Integer, AbstractClauseCollection> pairedRdd = inputCNF.mapToPair(new PairFunction<int[], Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Tuple2<Integer, AbstractClauseCollection> call(int[] arg0) throws Exception {
//				// TODO Auto-generated method stub
//				AbstractClauseCollection cc = new ZBDDClauseCollection(i, arg0);
//				return new Tuple2(cc.getKey(), cc);
//			}
//			
//		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){
//
//			@Override
//			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
//				// TODO Auto-generated method stub
//				return arg0._1() != 0;
//			}
//			
//		});
//		
//		return pairedRdd;
//	}

	protected static JavaPairRDD<Integer, BucketClause> initializePairedBucketClauses(JavaRDD<int[]> inputCNF) {
		// TODO Auto-generated method stub
		JavaPairRDD<Integer, BucketClause> pairedRdd = inputCNF.mapToPair(new PairFunction<int[], Integer, BucketClause>(){

			@Override
			public Tuple2<Integer, BucketClause> call(int[] arg0) throws Exception {
				// TODO Auto-generated method stub
				BucketClause clause = new BucketClause(arg0);
				return new Tuple2(clause.getKey(), clause);
			}
			
		}).filter(new Function<Tuple2<Integer, BucketClause>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, BucketClause> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._1() != 0;
			}
			
		});
		
		return pairedRdd;
	}
	
	protected static AbstractClauseCollection initiateClauseCollection(int datastructure_mode, int iteration_number) {
		// TODO Auto-generated method stub
		AbstractClauseCollection initialClauseCollection = null;
		
		switch (datastructure_mode) {
		case 1:
			initialClauseCollection = new SetClauseCollection(iteration_number);
			break;
		case 2:
			initialClauseCollection = new TrieClauseCollection(iteration_number);
			break;
		case 3:
			initialClauseCollection = new MyZBDDClauseCollection(iteration_number);
			break;
		default:
			throw new RuntimeException("wrong type!!!");
		}
		
		return initialClauseCollection;
	}

//	protected static Integer[] loadOrders(int num_vars) {
//		// TODO Auto-generated method stub
//		Integer[] orders = new Integer[num_vars];
//	
//		for(int i = 0; i < num_vars; i++){
//			orders[i] = i + 1;
//		}
//
//		return orders;
//	}
	
//	protected static Integer[] loadOrders(File ordering_input) {
//		// TODO Auto-generated method stub
//		Integer[] orders = null;
//		BufferedReader reader = null;
//		try {
//			String line;
//			reader = new BufferedReader(new FileReader(ordering_input));
//			while((line = reader.readLine()) != null){
//				orders = new Integer[Integer.parseInt(line)];
////				for(int i = 0; i < orders.length && (line = reader.readLine()) != null; i++){
////					orders[i] = Integer.parseInt(line);
////				}
//				for(int i = 0; i < orders.length; i++){
////					orders[i] = orders.length - i;
//					orders[i] = i + 1;
//				}
//				break;
//			}
//			reader.close();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally{
//			if(reader != null){
//				try {
//					reader.close();
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		}
//		
//
//		return orders;
//	}
	
	protected static JavaPairRDD<Integer, BucketClause> mapToPair_resolution(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<BucketClause, BucketClause>> resolvedRdd) {
		JavaPairRDD<Integer, BucketClause> resultingRdd = resolvedRdd.mapToPair(new PairFunction<Tuple2<Integer, Tuple2<BucketClause, BucketClause>>, Integer, BucketClause>() {
			
			@Override
			public Tuple2<Integer, BucketClause> call(Tuple2<Integer, Tuple2<BucketClause, BucketClause>> arg0)
					throws Exception {
				// TODO Auto-generated method stub
				BucketClause resolvedClause = doResolution(arg0);
				if (resolvedClause.isFalse()) {
					accumulator.add(1);
				}
				return new Tuple2(resolvedClause.getKey(), resolvedClause);
			}

			private BucketClause doResolution(Tuple2<Integer, Tuple2<BucketClause, BucketClause>> tuple) {
				Tuple2<BucketClause, BucketClause> pair = tuple._2();
				return new BucketClause(pair._1(), pair._2());
			}

		});
		
		return resultingRdd;
	}
	
	
	
	
	protected static void printOutPartitionsCollection(JavaPairRDD<Integer, AbstractClauseCollection> pairedRdd){
		pairedRdd.foreachPartition(new VoidFunction<Iterator<Tuple2<Integer, AbstractClauseCollection>>>(){

			@Override
			public void call(Iterator<Tuple2<Integer, AbstractClauseCollection>> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(!arg0.hasNext()){
					System.out.println("[]");
					return;
				}
				
				System.out.print("[");
				while(arg0.hasNext()){
					System.out.print(arg0.next());
				}
				System.out.println("]");
			}});
		System.out.println();
	}
	
	
	protected static void printOutPartitionsTupleCollection(JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedPairedRdd) {
		//for debugging
		resolvedPairedRdd.foreachPartition(new VoidFunction<Iterator<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>>>(){

			@Override
			public void call(Iterator<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(!arg0.hasNext()){
					System.out.println("[]");
					return;
				}
				
				System.out.print("[");
				while(arg0.hasNext()){
					System.out.print(arg0.next());
				}
				System.out.println("]");
			}});
		System.out.println();
	}
	
	
	protected static void printoutPartitionsTupleClause(JavaRDD<Tuple2<BucketClause, BucketClause>> pairedRdd) {
		// TODO Auto-generated method stub
		pairedRdd.foreachPartition(new VoidFunction<Iterator<Tuple2<BucketClause, BucketClause>>>(){

			@Override
			public void call(Iterator<Tuple2<BucketClause, BucketClause>> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(!arg0.hasNext()){
					System.out.println("[]");
					return;
				}
				
				System.out.print("[");
				while(arg0.hasNext()){
					System.out.print(arg0.next());
				}
				System.out.println("]");
			}});
		System.out.println();
	}

	protected static void printOutPartitionsClause(JavaPairRDD<Integer, BucketClause> pairedRdd) {
		pairedRdd.foreachPartition(new VoidFunction<Iterator<Tuple2<Integer, BucketClause>>>(){

			@Override
			public void call(Iterator<Tuple2<Integer, BucketClause>> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(!arg0.hasNext()){
					System.out.println("[]");
					return;
				}
				
				System.out.print("[");
				while(arg0.hasNext()){
					System.out.print(arg0.next());
				}
				System.out.println("]");
			}});
		System.out.println();
	}

	protected static void printOutPartitionsTupleClause(JavaPairRDD<Integer, Tuple2<BucketClause, BucketClause>> resolvedPairedRdd) {
		//for debugging
		resolvedPairedRdd.foreachPartition(new VoidFunction<Iterator<Tuple2<Integer, Tuple2<BucketClause, BucketClause>>>>(){

			@Override
			public void call(Iterator<Tuple2<Integer, Tuple2<BucketClause, BucketClause>>> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(!arg0.hasNext()){
					System.out.println("[]");
					return;
				}
				
				System.out.print("[");
				while(arg0.hasNext()){
					System.out.print(arg0.next());
				}
				System.out.println("]");
			}});
	}
	
}
