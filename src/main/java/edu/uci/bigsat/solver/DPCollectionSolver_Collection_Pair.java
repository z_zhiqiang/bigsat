package edu.uci.bigsat.solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.storage.StorageLevel;

import edu.uci.bigsat.BigSAT;
import edu.uci.bigsat.datastructure.AbstractClauseCollection;
import edu.uci.bigsat.datastructure.BucketClause;
import edu.uci.bigsat.datastructure.MyZBDDClauseCollection;
import edu.uci.bigsat.datastructure.zbdd.MyZDDManager;
import edu.uci.bigsat.datastructure.zbdd.MyZDDNode;
import edu.uci.bigsat.util.MyPartitioner;
import scala.Tuple2;

public class DPCollectionSolver_Collection_Pair extends AbstractSolver {

	private static AbstractSolver instance = new DPCollectionSolver_Collection_Pair();
	
	private DPCollectionSolver_Collection_Pair(){}
	
	public static AbstractSolver getInstance(){
		return instance;
	}
	
	
	@Override
	public void doResolution(int datastructure_mode, JavaSparkContext jsc, final int number_buckets, JavaRDD<int[]> inputCNF, final int number_partitions, final int number_splits) {
		// TODO Auto-generated method stub
		System.out.println("Parallel DP");
		
		//initiate an accumulator for early termination once there's an empty clause
		final Accumulator<Integer> earlyTerminationAccumulator = jsc.intAccumulator(0);
		
		//create paired cnf rdd
		JavaPairRDD<Integer, BucketClause> pairedCNF = initializePairedBucketClauses(inputCNF).persist(StorageLevel.MEMORY_ONLY());
//		System.out.println(pairedCNF.collect());
		
		//filter out positive clauses and aggregate them into clauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> collectionRdd = aggregate(datastructure_mode, pairedCNF, 1)
				.partitionBy(new MyPartitioner(number_partitions))
				;
		printOutPartitioner(collectionRdd, "pos");
//		printOutPartitionsCollection(collectionRdd);
		
		int empty_flag = 0;
		for(int i = 1; true; i++){
			final int iteration = i;
			System.out.println("\nIteration:\t" + iteration);
//			System.out.println("Counts:\t" + BigSAT.tcount);
			
			//join clauses
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD = collectionRdd.join(collectionRdd);
//			printOutPartitioner(resolvedRDD, "join");
			
			//filter out the non-dirty pairs of clauseCollection
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD = filterPairs(iteration, resolvedRDD);
//			printOutPartitioner(validResolvedRDD, "filterPair");
//			printOutPartitionsTupleCollection(validResolvedRDD);
			
			//---------------------------------------------------
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> newValidResolvedRDD = distributePair(iteration, validResolvedRDD, number_splits)
					.partitionBy(new MyPartitioner(number_partitions));
//			printOutPartitionsTupleCollection(newValidResolvedRDD);
			
			JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = doRESCollection(earlyTerminationAccumulator, newValidResolvedRDD, iteration + 1).cache();
//			printOutPartitionsCollection(resultingPairRDD);
			
			//---------------------------------------------------
			//termination check
			System.out.println("Accumulator:\t" + earlyTerminationAccumulator.value());
			if(earlyTerminationAccumulator.value() > 0){
				System.err.println("Finished >>>>>> UNSAT!!!");
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
			if(resultingPairRDD.isEmpty()){
				empty_flag++;
			}
			if(empty_flag >= 2){
				System.out.println();
				System.err.println("Finished >>>>>> SAT!!!");
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
			//---------------------------------------------------
			
			JavaPairRDD<Integer, AbstractClauseCollection> newCollectionRdd = reduceCollections(resultingPairRDD)
					.partitionBy(new MyPartitioner(number_partitions))
					;
//			printOutPartitioner(newCollectionRdd, "union new");
//			printOutPartitionsCollection(newCollectionRdd);
			
			collectionRdd = filterBucketsAndReduceCollections(collectionRdd, iteration).union(newCollectionRdd).cache();
//			printOutPartitioner(collectionRdd, "union");
//			printOutPartitionsCollection(collectionRdd);
		}
	}

	private JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> distributePair(final int iteration,
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD, final int num_splits) {
		JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> newValidResolvedRDD = validResolvedRDD.flatMapToPair(
				new PairFlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>(){

					@Override
					public Iterable<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>> call(
							Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t)
							throws Exception {
						assert(t._2()._1().getIteration_number() == iteration);
						return MyZBDDClauseCollection.splitResolution(t._2()._1(), t._2()._2(), num_splits);
						
					}});
		
		
		return newValidResolvedRDD;
	}

//	private static JavaPairRDD<Integer, AbstractClauseCollection> renameNewClauseCollection(
//			JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD,
//			final Broadcast<Long> broad_total_count_clause, final int number_buckets) {
//		resultingPairRDD = resultingPairRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, AbstractClauseCollection>, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(
//					Tuple2<Integer, AbstractClauseCollection> t) throws Exception {
//				// TODO Auto-generated method stub
//				long cut_count = needToCutCollection(t._2(), broad_total_count_clause, number_buckets);
////				System.out.println(cut_count);
//				if(cut_count != 0){
//					return t._2().renamePartialCollection(number_buckets, cut_count);
//				}
//				else{
//					List<Tuple2<Integer, AbstractClauseCollection>> list = new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
//					list.add(t);
//					return list;
//				}
//			}});
//		return resultingPairRDD;
//	}
//	
//
//	/**decide if a clauseCollection needs to be cut, if so, return the count to be cut
//	 * @param clauseCollection
//	 * @param broad_total_count_clause
//	 * @param number_buckets
//	 * @return
//	 */
//	private static long needToCutCollection(AbstractClauseCollection clauseCollection, final Broadcast<Long> broad_total_count_clause, final int number_buckets) {
//		// TODO Auto-generated method stub
////		if(clauseCollection.getKey() > number_buckets){
////			return 0;
////		}
//		
//		MyZDDNode zdd = ((MyZBDDClauseCollection) clauseCollection).getZdd();
//		long count = zdd.count();
//		
//		
//		long threshold_count = 500000;
//		double factor = 1;
//		long average_count = broad_total_count_clause.getValue() / number_buckets;
//		long base = (long) (average_count * factor);
//		
//		if(count > base && count > threshold_count){
//			System.err.println("\n" + clauseCollection.getKey() + "\t" + count + "\tcutting clauseeeeeeeeeeeeeeeeeeeeeeeeeeee");
//			return count - base;
//		}
//		
//		return 0;
//	}
//
//	
//	/**get the total number of clauses
//	 * @param jsc
//	 * @param resultingPairRDD
//	 * @return
//	 */
//	private static Broadcast<Long> getTotalCount_clause(JavaSparkContext jsc, JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD) {
//		JavaPairRDD<Integer, Long> validCountsRDD = resultingPairRDD.mapValues(new Function<AbstractClauseCollection, Long>(){
//
//			@Override
//			public Long call(AbstractClauseCollection v1) throws Exception {
//				// TODO Auto-generated method stub
//				return (long) v1.getNumClauses();
//			}})
//				.reduceByKey(new Function2<Long, Long, Long>() {
//
//					@Override
//					public Long call(Long v1, Long v2) throws Exception {
//						return v1 + v2;
//					}
//				})
//					;
//		
//		System.out.println(validCountsRDD.sortByKey(true).collect());
//		
//		long total_count = validCountsRDD.values().reduce(new Function2<Long, Long, Long>(){
//
//			@Override
//			public Long call(Long v1, Long v2) throws Exception {
//				// TODO Auto-generated method stub
//				return v1 + v2;
//			}});
//		System.out.println("Total clause count: " + total_count);
//		return jsc.broadcast(total_count);
//	}
//
//	
//	/**get the total number of resolution need to be done
//	 * @param jsc
//	 * @param validResolvedRDD
//	 * @param iteration 
//	 * @return
//	 */
//	private static Broadcast<Long> getTotalCount_resolution(JavaSparkContext jsc, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD, final int iteration) {
//		JavaPairRDD<Integer, Long> validCountsRDD = validResolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, Long>(){
//
//			@Override
//			public Long call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> v1) throws Exception {
//				// TODO
//				if(v1._1().getIteration_number() != iteration){
//					return (long) 0;
//				}
//				return v1._1().countResolutions(v1._2());
//			}})
////				.reduceByKey(new Function2<Long, Long, Long>(){
////
////				@Override
////				public Long call(Long v1, Long v2) throws Exception {
////					return v1 + v2;
////				}})
//;
//		System.out.println(validCountsRDD.sortByKey(true).collect());
//		
//		long total_count = validCountsRDD.values().reduce(new Function2<Long, Long, Long>(){
//
//			@Override
//			public Long call(Long v1, Long v2) throws Exception {
//				return v1 + v2;
//			}});
//		
//		System.out.println("Total resolution count: " + total_count);
//		return jsc.broadcast(total_count);
//	}
//	
//	
//	/**resolution with load balancing
//	 * @param accumulator
//	 * @param validResolvedRDD
//	 * @param iteration_num_new
//	 * @param broad_total_count
//	 * @param number_buckets
//	 * @param cut_percent
//	 * @return
//	 */
//	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection_Balance(final Accumulator<Integer> accumulator,
//			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD, final int iteration_num_new,
//			final Broadcast<Long> broad_total_count, final int number_buckets, final double cut_percent) {
//		// TODO Auto-generated method stub
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = validResolvedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(
//					Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
//				// TODO Auto-generated method stub
////				System.err.println("flatmap\t" + t._1());
//				if(t._2()._1().getIteration_number() != iteration_num_new - 1){
//					return new ArrayList<Tuple2<Integer, AbstractClauseCollection>>();
//				}
//				
//				if(needToCutPair(t._2(), broad_total_count, number_buckets)){
////					System.err.println(t._1());
//					return t._2()._1().doResolutionPair_balance(t._2()._2(), accumulator, iteration_num_new, cut_percent, number_buckets);
//				}
//				else{
//					return t._2()._1().doResolutionPair(t._2()._2(), accumulator, iteration_num_new);
//				}
//				
//			}});
//		
//		return resultingPairRDD;
//	}
//	
//	
//	/**combine all old clauseCollections, each key one ClauseCollection; delete renamed clauses and add additional extended clauses for renaming
//	 * @param accumulator
//	 * @param validResolvedRDD
//	 * @param iteration_num
//	 * @param broad_total_count
//	 * @param cut_percent 
//	 * @return
//	 */
//	private static JavaPairRDD<Integer, AbstractClauseCollection> renameAndCombineCollection(Accumulator<Integer> accumulator,
//			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD, final int iteration_num,
//			final Broadcast<Long> broad_total_count, final int number_buckets, final double cut_percent) {
//		JavaPairRDD<Integer, AbstractClauseCollection> collectionRdd = validResolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, AbstractClauseCollection>(){
//
//			@Override
//			public AbstractClauseCollection call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> v1)
//					throws Exception {
////				System.err.println("mapvalues\t" + v1._1().getKey());
//				if(v1._1().getIteration_number() != iteration_num){
//					return v1._1();
//				}
//				//return all the clauseCollections after renaming if applicable
//				if(needToCutPair(v1, broad_total_count, number_buckets)){
//					//TODO
//					//this's highly correlated to the map function "doResolutionPair_balance" in "doRESCollection_Balance"
//					return v1._1().renameAndcombinePair(v1._2(), cut_percent, number_buckets);
//				}
//				else{
//					//just combine clauseCollections in the pair
//					return v1._1().combinePair(v1._2());
//				}
//			}});
//		
//		return collectionRdd;
//	}
//	
//	
//	/**decide if the resolution of a pair needs to be cut
//	 * @param tuple2
//	 * @param broad_total_count
//	 * @param number_buckets
//	 * @return
//	 */
//	private static boolean needToCutPair(Tuple2<AbstractClauseCollection,AbstractClauseCollection> tuple2,
//			Broadcast<Long> broad_total_count, int number_buckets) {
//		//TODO
////		if(tuple2._1().getKey() > number_buckets){
////			return false;
////		}
//
//		long threshold_count = 1000000000;
//		long factor = (long) (0 * number_buckets);
//		long resolution_count = tuple2._1().countResolutions(tuple2._2());
//		long average_count = broad_total_count.getValue() / number_buckets;
////		if(resolution_count > average_count * factor && resolution_count > threshold_count){
////			System.err.println("\n" + tuple2._1().getKey() + "\t" + resolution_count + "\tcutting pairrrrrrrrrrrrrrrrrrrrrrrrrrr");
////			return true;
////		}
//		return false;
//	}


	private static JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> filterPairs(final int iteration, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD) {
		return resolvedRDD.filter(new Function<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._2()._1().getIteration_number() == iteration;
			}
			
		}).reduceByKey(new Function2<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, Tuple2<AbstractClauseCollection, AbstractClauseCollection>, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>(){

			@Override
			public Tuple2<AbstractClauseCollection, AbstractClauseCollection> call(
					Tuple2<AbstractClauseCollection, AbstractClauseCollection> v1,
					Tuple2<AbstractClauseCollection, AbstractClauseCollection> v2) throws Exception {
				// TODO Auto-generated method stub
				if(v1._1().getIteration_number() > v1._2().getIteration_number()){
					return v1;
				}
				else{
					assert(v2._1().getIteration_number() > v2._2().getIteration_number());
					return v2;
				}
			}})
				.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>(){

				@Override
				public Tuple2<AbstractClauseCollection, AbstractClauseCollection> call(
						Tuple2<AbstractClauseCollection, AbstractClauseCollection> v1) throws Exception {
					// TODO Auto-generated method stub
					if(v1._1().getIteration_number() > v1._2().getIteration_number()){
						return MyZBDDClauseCollection.removeDuplicates(v1._1(), v1._2());
					}
					return v1;
				}})
				.filter(new Function<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Boolean>(){

					@Override
					public Boolean call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> arg0) throws Exception {
						// TODO Auto-generated method stub
						return arg0._2()._1().getIteration_number() == iteration;
					}
					
				})
				
				;
	}


	/**filter out the finished bucket, and reduce all clauseCollections of old iteration
	 * @param clauseCollectionCNF
	 * @param iteration
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterBucketsAndReduceCollections(JavaPairRDD<Integer, AbstractClauseCollection> clauseCollectionCNF, final int iteration) {
		return clauseCollectionCNF
				.filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>() {

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> v1) throws Exception {
				// TODO Auto-generated method stub
				return v1._1() > iteration;
			}
		})
				.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
					throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(v1, v2);
			}})
				;
	}
	
	private static JavaPairRDD<Integer, AbstractClauseCollection> reduceCollections(JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD) {
		
		JavaPairRDD<Integer, AbstractClauseCollection> cnfClauseCollections = resultingPairRDD.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
					throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(v1, v2);
			}
			
		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				return !arg0._2().isEmpty();
			}
			
		});
		
		
		return cnfClauseCollections;
	}
	
	
	/**
	 * @param resultingPairRDD
	 * @param flag
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> aggregate(int datastructure_mode, JavaPairRDD<Integer, BucketClause> resultingPairRDD, final int iteration_num) {
		AbstractClauseCollection initialClauseCollection = initiateClauseCollection(datastructure_mode, iteration_num);

		//aggregate clauses with the same key into ClauseCollection and filter out empty ClauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> cnfClauseCollections = resultingPairRDD.aggregateByKey(initialClauseCollection, new Function2<AbstractClauseCollection, BucketClause, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, BucketClause arg1) throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClause(arg0, arg1);
			}
			
		}, new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, AbstractClauseCollection arg1) throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(arg0, arg1);
			}
			
		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				return !arg0._2().isEmpty();
			}
			
		});
		
		return cnfClauseCollections;
	}
	

//	/**
//	 * @param resolvedRDD
//	 * @param iteration_num 
//	 * @param orders
//	 * @return
//	 */
//	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection_Pair(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD, final int iteration_num) {
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = validResolvedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(
//					Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
//				// TODO Auto-generated method stub
//				return t._2()._1().doResolutionPair(t._2()._2(), accumulator, iteration_num);
//			}});
//		
//		return resultingPairRDD;
//	}
	
	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD, final int iteration_num) {
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingRDD = resolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, AbstractClauseCollection>(){
//
//			@Override
//			public AbstractClauseCollection call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> t) throws Exception {
//				// TODO Auto-generated method stub
//				return t._1().doResolutionCollection_noFirst(t._2(), accumulator);
//			}
//			
//		});
//		printOutPartitioner(resultingRDD, "res1");
//
//		resultingRDD = resultingRDD.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){
//
//			@Override
//			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
//					throws Exception {
//				return v1.addClauses(v2);
//			}});
//		printOutPartitioner(resultingRDD, "res2");
//		
////		//for debugging mode
////		JavaPairRDD<Integer, AbstractClauseCollection> resultingRDD = resolvedRDD.mapToPair(new PairFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Integer, AbstractClauseCollection>(){
////
////			@Override
////			public Tuple2<Integer, AbstractClauseCollection> call(
////					Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
////				// TODO Auto-generated method stub
////				long stime = System.currentTimeMillis();
////				AbstractClauseCollection cc = t._2()._1().doResolutionCollection_noFirst(t._2()._2(), accumulator);
////				long etime = System.currentTimeMillis();
////				MyZBDDClauseCollection.resolutiontime += etime - stime;
////				System.err.println(t._1() + "\tProduct time:\t" + (etime - stime));
////				return new Tuple2<Integer, AbstractClauseCollection>(t._1(), cc);
////			}});
//		
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = resultingRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, AbstractClauseCollection>, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(Tuple2<Integer, AbstractClauseCollection> t)
//					throws Exception {
//				// TODO Auto-generated method stub
//				return t._2().splitBuckets(iteration_num);
//			}});
//		printOutPartitioner(resultingPairRDD, "res3");
		
		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = resolvedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Integer, AbstractClauseCollection>(){

			@Override
			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(
					Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
				long stime = System.currentTimeMillis();
				List<Tuple2<Integer, AbstractClauseCollection>> list = MyZBDDClauseCollection.doResolutionCollection_noFirst(t._2()._1(), t._2()._2(), accumulator, iteration_num);
				long etime = System.currentTimeMillis();
				BigSAT.resolutiontime += etime - stime;
//				System.err.println(t._1() + "\tProduct time:\t" + (etime - stime));
				
				return list;
			}});
		
		return resultingPairRDD;
	}
	
	
	/**print out partitioner information
	 * @param rdd
	 * @param point
	 */
	private static void printOutPartitioner(JavaPairRDD rdd, String point){
		if(rdd.partitioner().isPresent()){
			System.out.println(point + ":\t" + rdd.partitioner().get());
		}
		else{
			System.out.println(point + ":\t" + false);
		}
	}

}
