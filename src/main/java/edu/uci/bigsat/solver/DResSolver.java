package edu.uci.bigsat.solver;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.storage.StorageLevel;

import com.google.common.base.Optional;

import edu.uci.bigsat.datastructure.BucketClause;
import edu.uci.bigsat.datastructure.MultiClause;
import scala.Tuple2;

public class DResSolver extends AbstractSolver {

	private static AbstractSolver instance = new DResSolver();
	
	private DResSolver(){}
	
	public static AbstractSolver getInstance(){
		return instance;
	}
	
	@Override
	public void doResolution(int datastructure_mode, JavaSparkContext jsc, final int number_buckets, JavaRDD<int[]> inputCNF, final int number_partitions, final int number_splits) {
		// TODO Auto-generated method stub
		System.out.println("Distributed Resolution");
		
		//initiate an accumulator for early termination once there's an empty clause
		final Accumulator<Integer> earlyTerminationAccumulator = jsc.intAccumulator(0);
		
		//create paired cnf rdd
		JavaPairRDD<Integer, MultiClause> pairedCNF = initializePairedMultiClauses(inputCNF).persist(StorageLevel.MEMORY_ONLY());
		
		//generate rdd of clauses whose key literal is positive. e.g., clause <1, 2> for key 1
		JavaPairRDD<Integer, MultiClause> positivePairedCNF = pairedCNF.filter(new Function<Tuple2<Integer, MultiClause>, Boolean>(){
			
			@Override
			public Boolean call(Tuple2<Integer, MultiClause> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._2.isPositive();
			}
			
		});
		
		//generate rdd of clauses whose key literal is negative. e.g., clause <-1, 2> for key 1
		JavaPairRDD<Integer, MultiClause> negativePairedCNF = pairedCNF.filter(new Function<Tuple2<Integer, MultiClause>, Boolean>(){
			
			@Override
			public Boolean call(Tuple2<Integer, MultiClause> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._2.isNegative();
			}
			
		});
		
		//join to get all the pairs of clauses over which resolution can be performed
		JavaPairRDD<Integer,Tuple2<Optional<MultiClause>,Optional<MultiClause>>> resolvedRdd = positivePairedCNF.fullOuterJoin(negativePairedCNF);
		
//		JavaPairRDD<Integer, MultiClause> newRdd = resolvedRdd.mapToPair(new PairFunction<Tuple2<Integer, Tuple2<Optional<MultiClause>, Optional<MultiClause>>>, Integer, MultiClause>(){
//			
//			@Override
//			public Tuple2<Integer, MultiClause> call(Tuple2<Integer, Tuple2<Optional<MultiClause>, Optional<MultiClause>>> arg0)
//					throws Exception {
//				// TODO Auto-generated method stub
//				MultiClause resolvedClause = doResolution(arg0);
//				return new Tuple2(resolvedClause.getKey(), resolvedClause);
//			}
//			
//			private MultiClause doResolution(Tuple2<Integer,Tuple2<Optional<MultiClause>,Optional<MultiClause>>> tuple) {
//				Tuple2<Optional<MultiClause>,Optional<MultiClause>> pair = tuple._2();
//				// TODO Auto-generated method stub
//				if(pair._1().isPresent() && pair._2().isPresent()){
////					return new Clause(pair._1().get(), pair._2().get(), tuple._1(), orders);
//					return null;
//				}
//				else if(pair._1().isPresent()){
//					return pair._1().get();
//				}
//				else if(pair._2().isPresent()){
//					return pair._2().get();
//				}
//				else{
//					throw new RuntimeException("wrong case!!!");
//				}
//			}
//			
//		});
	}
	

	protected static JavaPairRDD<Integer, MultiClause> initializePairedMultiClauses(JavaRDD<int[]> cnf_string) {
//		JavaPairRDD<Integer, MultiClause> pairedRdd = cnf_string.flatMapToPair(new PairFlatMapFunction<String, Integer, MultiClause>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, MultiClause>> call(String t) throws Exception {
//				// TODO Auto-generated method stub
//				List<Tuple2<Integer, MultiClause>> rList = new ArrayList<Tuple2<Integer, MultiClause>>();
//				List<Integer> literals = new ArrayList<Integer>();
//				byte indicator = BucketClause.initializeLiterals(literals, t);
//				assert(indicator == 0);
//				
//				for(int literal: literals){
//					boolean flag = literal > 0 ? true : false;
//					MultiClause clause;
//					if(flag){
//						clause = new MultiClause(literals, literal, (byte) 2);
//					}
//					else{
//						clause = new MultiClause(literals, 0 - literal, (byte) -2);
//					}
//					rList.add(new Tuple2<Integer, MultiClause>(clause.getKey(), clause));
//				}
//				
//				return rList;
//			}
//			
//		});
//				
//		return pairedRdd;
		return null;
	}

}
