package edu.uci.bigsat.solver;

import java.util.List;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.storage.StorageLevel;

import edu.uci.bigsat.datastructure.BucketClause;
import edu.uci.bigsat.datastructure.SetClauseCollection;
import edu.uci.bigsat.BigSAT;
import edu.uci.bigsat.datastructure.AbstractClauseCollection;
import scala.Tuple2;

public class DPCollectionSolver extends AbstractSolver {

	private static AbstractSolver instance = new DPCollectionSolver();
	
	private DPCollectionSolver(){}
	
	public static AbstractSolver getInstance(){
		return instance;
	}
	
	
	@Override
	public void doResolution(int datastructure_mode, JavaSparkContext jsc, final int number_buckets, JavaRDD<int[]> inputCNF, final int number_partitions, final int number_splits) {
		// TODO Auto-generated method stub
		System.out.println("Parallel DP");
		
		//initiate an accumulator for early termination once there's an empty clause
		final Accumulator<Integer> earlyTerminationAccumulator = jsc.intAccumulator(0);
		
		//create paired cnf rdd
		JavaPairRDD<Integer, BucketClause> pairedCNF = initializePairedBucketClauses(inputCNF).persist(StorageLevel.MEMORY_ONLY());
		System.out.println(pairedCNF.collect());
		
		//filter out positive clauses and aggregate them into clauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> positiveRdd = filterAndAggregate(datastructure_mode, pairedCNF, true, 1);
		//filter out negative clauses and aggregate them into clauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> negativeRdd = filterAndAggregate(datastructure_mode, pairedCNF, false, 1);
		
		for(int i = 1; i <= number_buckets; i++){
			final int iteration = i;
			System.out.println("Iteration:\t" + iteration);
//			System.out.println("Counts:\t" + BigSAT.tcount);
			
			//print out the statistics data
			JavaPairRDD<Integer, Long> countsPositiveRdd = getStatisticsData(positiveRdd);
			System.out.println("Positive: " + countsPositiveRdd.collect());
			JavaPairRDD<Integer, Long> countsNegativeRdd = getStatisticsData(negativeRdd);
			System.out.println("Negative: " + countsNegativeRdd.collect());
			
			//join clauses
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD = positiveRdd.join(negativeRdd);
			
			//filter out the non-dirty pairs of clauseCollection
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD = filterPairs(iteration, resolvedRDD);
			
			//do resolution
			JavaPairRDD<Integer, BucketClause> resultingPairRDD = doRES(earlyTerminationAccumulator, validResolvedRDD, iteration).cache();
			
			System.out.println("Accumulator:\t" + earlyTerminationAccumulator);
			if(earlyTerminationAccumulator.value() > 0){
				System.err.println("Finished >>>>>> UNSAT!!!");
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
			if(resultingPairRDD.isEmpty()){
				System.out.println();
				System.err.println("Finished >>>>>> SAT!!!");
//				System.out.println("Serialization time:\t" + MyZBDDClauseCollection.time / 1000);
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
			JavaPairRDD<Integer, AbstractClauseCollection> newPositiveRdd = filterAndAggregate(datastructure_mode, resultingPairRDD, true, i + 1);
			positiveRdd = filterBucketsAndReduceCollections(positiveRdd, iteration).union(newPositiveRdd);
			
			JavaPairRDD<Integer, AbstractClauseCollection> newNegativeRdd = filterAndAggregate(datastructure_mode, resultingPairRDD, false, i + 1);
			negativeRdd = filterBucketsAndReduceCollections(negativeRdd, iteration).union(newNegativeRdd);
		}
		
		System.out.println();
		System.err.println("Finished >>>>>> SAT!!!");
		jsc.cancelAllJobs();
		jsc.stop();
		return;
	}

	private static JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> filterPairs(final int iteration, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD) {
		return resolvedRDD.filter(new Function<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._2()._1().getIteration_number() == iteration || arg0._2()._2().getIteration_number() == iteration;
			}
			
		});
	}
	

	/**
	 * @param setCNF
	 * @param iteration
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterBucketsAndReduceCollections(JavaPairRDD<Integer, AbstractClauseCollection> setCNF, final int iteration) {
		return setCNF
				.filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>() {

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> v1) throws Exception {
				// TODO Auto-generated method stub
				return v1._1() > iteration;
			}
		})
				.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

					@Override
					public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
							throws Exception {
						// TODO Auto-generated method stub
						return SetClauseCollection.addClauses(v1, v2);
					}
					
				})
				;
	}
	
	/**
	 * @param resultingPairRDD
	 * @param flag
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterAndAggregate(int datastructure_mode, JavaPairRDD<Integer, BucketClause> resultingPairRDD, final boolean flag, final int iteration_num) {
		//generate rdd of clauses whose key literal is negative (e.g., clause <-1, 2> for key 1) when flag is false or positive (e.g., clause <1, 2> for key 1) when flag is true
		JavaPairRDD<Integer, BucketClause> clauseRdd = resultingPairRDD.filter(new Function<Tuple2<Integer, BucketClause>, Boolean>(){
			
			@Override
			public Boolean call(Tuple2<Integer, BucketClause> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(flag){
					return arg0._2().isPositive();
				}
				else{
					return arg0._2().isNegative();
				}
			}
			
		});
		
		AbstractClauseCollection initialClauseCollection = initiateClauseCollection(datastructure_mode, iteration_num);

		//aggregate clauses with the same key into SetClauses and filter out empty SetClauses
		JavaPairRDD<Integer, AbstractClauseCollection> cnfClauses = clauseRdd.aggregateByKey(initialClauseCollection, new Function2<AbstractClauseCollection, BucketClause, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, BucketClause arg1) throws Exception {
				// TODO Auto-generated method stub
				return SetClauseCollection.addClause(arg0, arg1);
			}
			
		}, new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, AbstractClauseCollection arg1) throws Exception {
				// TODO Auto-generated method stub
				return SetClauseCollection.addClauses(arg0, arg1);
			}
			
		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				return !arg0._2().isEmpty();
			}
			
		});
		
		return cnfClauses;
	}

	/**
	 * @param resolvedRDD
	 * @param iteration 
	 * @param orders
	 * @return
	 */
	private static JavaPairRDD<Integer, BucketClause> doRES(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD, int iteration) {
		JavaRDD<BucketClause> resultingRDD = resolvedRDD.flatMap(new FlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, BucketClause>(){

			@Override
			public Iterable<BucketClause> call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
				// TODO Auto-generated method stub
				List<BucketClause> clause_list = SetClauseCollection.doResolution(t._2()._1(), t._2()._2(), accumulator);
//				if(iteration == 11)
//					System.out.println("resolution " + iteration);
				return clause_list;
			}
			
		});
		
		JavaPairRDD<Integer, BucketClause> resultingPairRDD = resultingRDD.mapToPair(new PairFunction<BucketClause, Integer, BucketClause>(){

			@Override
			public Tuple2<Integer, BucketClause> call(BucketClause arg0) throws Exception {
				// TODO Auto-generated method stub
				return new Tuple2<Integer, BucketClause>(arg0.getKey(), arg0);
			}
			
		});
		
		return resultingPairRDD;
	}
	


}
