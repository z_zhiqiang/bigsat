package edu.uci.bigsat.solver;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.spark.Accumulator;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.storage.StorageLevel;

import edu.uci.bigsat.BigSAT;
import edu.uci.bigsat.datastructure.AbstractClauseCollection;
import edu.uci.bigsat.datastructure.BucketClause;
import edu.uci.bigsat.datastructure.MyZBDDClauseCollection;
import edu.uci.bigsat.util.MyPartitioner;
import scala.Tuple2;

public class DPCollectionSolver_Collection extends AbstractSolver {

	private static AbstractSolver instance = new DPCollectionSolver_Collection();
	
	private DPCollectionSolver_Collection(){}
	
	public static AbstractSolver getInstance(){
		return instance;
	}
	
	
	@Override
	public void doResolution(int datastructure_mode, JavaSparkContext jsc, final int number_buckets, JavaRDD<int[]> inputCNF, final int number_partitions, final int number_splits) {
		// TODO Auto-generated method stub
		System.out.println("Parallel DP");
		
		//initiate an accumulator for early termination once there's an empty clause
		final Accumulator<Integer> earlyTerminationAccumulator = jsc.intAccumulator(0);
		
		//create paired cnf rdd
		JavaPairRDD<Integer, BucketClause> pairedCNF = initializePairedBucketClauses(inputCNF).persist(StorageLevel.MEMORY_ONLY());
//		System.out.println(pairedCNF.collect());
		printOutPartitionsClause(pairedCNF);
		
		
		//filter out positive clauses and aggregate them into clauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> positiveRdd = filterAndAggregate(datastructure_mode, pairedCNF, true, 1)
				.partitionBy(new MyPartitioner(number_partitions))
				;
		printOutPartitioner(positiveRdd, "pos");
//		printOutPartitionsCollection(positiveRdd);
		//filter out negative clauses and aggregate them into clauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> negativeRdd = filterAndAggregate(datastructure_mode, pairedCNF, false, 1)
				.partitionBy(new MyPartitioner(number_partitions))
				;
		printOutPartitioner(negativeRdd, "neg");
//		printOutPartitionsCollection(negativeRdd);
		
		
		for(int i = 1; true; i++){
			final int iteration = i;
			System.out.println("\nIteration:\t" + iteration);
//			System.out.println("Counts:\t" + BigSAT.tcount);
			
//			//print out the statistics data
//			JavaPairRDD<Integer, Integer> countsPositiveRdd = getStatisticsData(positiveRdd);
//			System.out.println("Positive: " + countsPositiveRdd.collect());
//			JavaPairRDD<Integer, Integer> countsNegativeRdd = getStatisticsData(negativeRdd);
//			System.out.println("Negative: " + countsNegativeRdd.collect());
			
			//join clauses
//			System.out.println("before joining");
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD = positiveRdd.join(negativeRdd);
			printOutPartitioner(resolvedRDD, "join");
//			System.out.println("end joining");
			
			//filter out the non-dirty pairs of clauseCollection
			JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> validResolvedRDD = filterPairs(iteration, resolvedRDD).cache();
			printOutPartitioner(resolvedRDD, "filterPair");
			
			//---------------------------------------------------
//			JavaPairRDD<Integer, Long> validCountsRDD = validResolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, Long>(){
//
//				@Override
//				public Long call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> v1) throws Exception {
//					// TODO Auto-generated method stub
//					return (((long) v1._1().getNumClauses()) * v1._2().getNumClauses());
//				}}).reduceByKey(new Function2<Long, Long, Long>(){
//
//					@Override
//					public Long call(Long v1, Long v2) throws Exception {
//						// TODO Auto-generated method stub
//						return v1 + v2;
//					}});
//			System.out.println(validCountsRDD.sortByKey(true).collect());
//			
//			JavaPairRDD<Integer, Long> partitionCountsRDD = validCountsRDD.mapToPair(new PairFunction<Tuple2<Integer, Long>, Integer, Long>(){
//
//				@Override
//				public Tuple2<Integer, Long> call(Tuple2<Integer, Long> t) throws Exception {
//					// TODO Auto-generated method stub
//					return new Tuple2<Integer, Long>(MyPartitioner.partition(t._1(), number_partitions), t._2());
//				}}).reduceByKey(new Function2<Long, Long, Long>(){
//
//					@Override
//					public Long call(Long v1, Long v2) throws Exception {
//						// TODO Auto-generated method stub
//						return v1 + v2;
//					}});
//			
//			System.out.println(partitionCountsRDD.collect());
//			
//			long total_count = partitionCountsRDD.values().reduce(new Function2<Long, Long, Long>(){
//
//				@Override
//				public Long call(Long v1, Long v2) throws Exception {
//					// TODO Auto-generated method stub
//					return v1 + v2;
//				}});
//			
//			System.out.println(total_count);
//			
//			final Broadcast<Long> broad_total_count = jsc.broadcast(total_count);
			
//			JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = doRESCollection(number_buckets, broad_total_count, earlyTerminationAccumulator, validResolvedRDD, i + 1).cache();
			//---------------------------------------------------
			
			//do resolution
//			System.out.println("before resolution");
			JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = doRESCollection(earlyTerminationAccumulator, validResolvedRDD, i + 1).cache();
//			printOutPartitionsCollection(resultingPairRDD);
//			print(resultingPairRDD);
//			System.out.println("Done resolution");
			
			long stime = System.currentTimeMillis();
//			System.out.println(resultingPairRDD.isEmpty());
			long etime = System.currentTimeMillis();
//			System.out.println(etime - stime);
			System.out.println("Accumulator:\t" + earlyTerminationAccumulator);
			if(earlyTerminationAccumulator.value() > 0){
				System.err.println("Finished >>>>>> UNSAT!!!");
//				System.out.println("Serialization time:\t" + MyZBDDClauseCollection.time / 1000);
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
			if(resultingPairRDD.isEmpty()){
				System.out.println();
				System.err.println("Finished >>>>>> SAT!!!");
//				System.out.println("Serialization time:\t" + MyZBDDClauseCollection.time / 1000);
				jsc.cancelAllJobs();
				jsc.stop();
				return;
			}
			
//			System.out.println("before filtering and reducing");
			JavaPairRDD<Integer, AbstractClauseCollection> newPositiveRdd = filterAndReduceCollections(resultingPairRDD, true)
					.partitionBy(new MyPartitioner(number_partitions));
			printOutPartitioner(newPositiveRdd, "union newpos");
//			printOutPartitionsCollection(newPositiveRdd);
			positiveRdd = filterBucketsAndReduceCollections(positiveRdd, iteration).union(newPositiveRdd);
			printOutPartitioner(positiveRdd, "union pos");
//			printOutPartitionsCollection(positiveRdd);
			
			JavaPairRDD<Integer, AbstractClauseCollection> newNegativeRdd = filterAndReduceCollections(resultingPairRDD, false)
					.partitionBy(new MyPartitioner(number_partitions));
			printOutPartitioner(newNegativeRdd, "union newneg");
//			printOutPartitionsCollection(newNegativeRdd);
			negativeRdd = filterBucketsAndReduceCollections(negativeRdd, iteration).union(newNegativeRdd);
			printOutPartitioner(negativeRdd, "union neg");
//			printOutPartitionsCollection(negativeRdd);
//			System.out.println("end filtering and reducing");
		}
		
	}
	
	private static void printOutPartitioner(JavaPairRDD rdd, String point){
		if(rdd.partitioner().isPresent()){
			System.out.println(point + ":\t" + rdd.partitioner().get());
		}
		else{
			System.out.println(point + ":\t" + false);
		}
	}

	private static JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> filterPairs(final int iteration, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD) {
		return resolvedRDD.filter(new Function<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> arg0) throws Exception {
				// TODO Auto-generated method stub
				return arg0._2()._1().getIteration_number() == iteration || arg0._2()._2().getIteration_number() == iteration;
			}
			
		});
	}


	/**filter out the finished bucket, and reduce all clauseCollections of old iteration
	 * @param clauseCollectionCNF
	 * @param iteration
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterBucketsAndReduceCollections(JavaPairRDD<Integer, AbstractClauseCollection> clauseCollectionCNF, final int iteration) {
		return clauseCollectionCNF
				.filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>() {

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> v1) throws Exception {
				// TODO Auto-generated method stub
				return v1._1() > iteration;
			}
		})
				.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
					throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(v1, v2);
			}})
				;
	}
	
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterAndReduceCollections(JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD, final boolean flag) {
		JavaPairRDD<Integer, AbstractClauseCollection> clauseCollectionRdd = resultingPairRDD.filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){
			
			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(flag){
					return arg0._2().isPositive();
				}
				else{
					return arg0._2().isNegative();
				}
			}
			
		});
		
		JavaPairRDD<Integer, AbstractClauseCollection> cnfClauseCollections = clauseCollectionRdd.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
					throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(v1, v2);
			}
			
		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				return !arg0._2().isEmpty();
//				return true;
			}
			
		});
		
		
		return cnfClauseCollections;
	}
	
	
	/**
	 * @param resultingPairRDD
	 * @param flag
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> filterAndAggregate(int datastructure_mode, JavaPairRDD<Integer, BucketClause> resultingPairRDD, final boolean flag, final int iteration_num) {
		//generate rdd of clauses whose key literal is negative (e.g., clause <-1, 2> for key 1) when flag is false or positive (e.g., clause <1, 2> for key 1) when flag is true
		JavaPairRDD<Integer, BucketClause> clauseRdd = resultingPairRDD.filter(new Function<Tuple2<Integer, BucketClause>, Boolean>(){
			
			@Override
			public Boolean call(Tuple2<Integer, BucketClause> arg0) throws Exception {
				// TODO Auto-generated method stub
				if(flag){
					return arg0._2().isPositive();
				}
				else{
					return arg0._2().isNegative();
				}
			}
			
		});
		
		AbstractClauseCollection initialClauseCollection = initiateClauseCollection(datastructure_mode, iteration_num);

		//aggregate clauses with the same key into ClauseCollection and filter out empty ClauseCollection
		JavaPairRDD<Integer, AbstractClauseCollection> cnfClauseCollections = clauseRdd.aggregateByKey(initialClauseCollection, new Function2<AbstractClauseCollection, BucketClause, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, BucketClause arg1) throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClause(arg0, arg1);
			}
			
		}, new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection arg0, AbstractClauseCollection arg1) throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.addClauses(arg0, arg1);
			}
			
		}).filter(new Function<Tuple2<Integer, AbstractClauseCollection>, Boolean>(){

			@Override
			public Boolean call(Tuple2<Integer, AbstractClauseCollection> arg0) throws Exception {
				// TODO Auto-generated method stub
				return !arg0._2().isEmpty();
//				return true;
			}
			
		});
		
		return cnfClauseCollections;
	}
	
//	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection(final int number_buckets, final Broadcast<Long> total_count, final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD, final int iteration_num) {
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingRDD = resolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, AbstractClauseCollection>(){
//
//			@Override
//			public AbstractClauseCollection call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> t) throws Exception {
//				// TODO Auto-generated method stub
//				if(((double) (t._1().getNumClauses() * t._2().getNumClauses())) / total_count.getValue() > 0.1){
//					System.err.println(t._1().getKey() + "\nstealing");
//					return t._1().doResolutionCollection(t._2(), number_buckets);
//				}
//				
//				return t._1().doResolutionCollection(t._2(), accumulator);
//			}
//			
//		});
//		printOutPartitioner(resultingRDD, "res1");
//		
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingReducedRDD = resultingRDD.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){
//
//			@Override
//			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
//					throws Exception {
//				// TODO Auto-generated method stub
////				v1.addClausesNoCleanup(v2);
//				return v1.addClauses(v2);
//			}});
//		printOutPartitioner(resultingReducedRDD, "res2");
//		
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = resultingReducedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, AbstractClauseCollection>, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(Tuple2<Integer, AbstractClauseCollection> t)
//					throws Exception {
//				// TODO Auto-generated method stub
//				return t._2().splitBuckets(iteration_num);
//			}});
//		printOutPartitioner(resultingPairRDD, "res3");
//		
//		return resultingPairRDD;
//	}

	/**
	 * @param resolvedRDD
	 * @param iteration_num 
	 * @param orders
	 * @return
	 */
	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD, final int iteration_num) {
		JavaPairRDD<Integer, AbstractClauseCollection> resultingRDD = resolvedRDD.mapValues(new Function<Tuple2<AbstractClauseCollection, AbstractClauseCollection>, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(Tuple2<AbstractClauseCollection, AbstractClauseCollection> t) throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.doResolutionCollection(t._1(), t._2(), accumulator);
			}
			
		});
		printOutPartitioner(resultingRDD, "res1");
		
		JavaPairRDD<Integer, AbstractClauseCollection> resultingReducedRDD = resultingRDD.reduceByKey(new Function2<AbstractClauseCollection, AbstractClauseCollection, AbstractClauseCollection>(){

			@Override
			public AbstractClauseCollection call(AbstractClauseCollection v1, AbstractClauseCollection v2)
					throws Exception {
				// TODO Auto-generated method stub
//				v1.addClausesNoCleanup(v2);
				return MyZBDDClauseCollection.addClauses(v1, v2);
			}});
		printOutPartitioner(resultingReducedRDD, "res2");
		
		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = resultingReducedRDD.flatMapToPair(new PairFlatMapFunction<Tuple2<Integer, AbstractClauseCollection>, Integer, AbstractClauseCollection>(){

			@Override
			public Iterable<Tuple2<Integer, AbstractClauseCollection>> call(Tuple2<Integer, AbstractClauseCollection> t)
					throws Exception {
				// TODO Auto-generated method stub
				return MyZBDDClauseCollection.splitBuckets(t._2(), iteration_num);
			}});
		printOutPartitioner(resultingPairRDD, "res3");
		
		return resultingPairRDD;
	}
	
//	/**
//	 * @param resolvedRDD
//	 * @param iteration_num 
//	 * @param orders
//	 * @return
//	 */
//	private static JavaPairRDD<Integer, AbstractClauseCollection> doRESCollection(final Accumulator<Integer> accumulator, JavaPairRDD<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> resolvedRDD, final int iteration_num) {
//		JavaRDD<AbstractClauseCollection> resultingRDD = resolvedRDD.flatMap(new FlatMapFunction<Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>>, AbstractClauseCollection>(){
//
//			@Override
//			public Iterable<AbstractClauseCollection> call(Tuple2<Integer, Tuple2<AbstractClauseCollection, AbstractClauseCollection>> t) throws Exception {
//				// TODO Auto-generated method stub
//				List<AbstractClauseCollection> clause_list = t._2()._1().doResolutionCollection(t._2()._2(), accumulator, iteration_num);
//				return clause_list;
//			}
//			
//		});
//		
//		JavaPairRDD<Integer, AbstractClauseCollection> resultingPairRDD = resultingRDD.mapToPair(new PairFunction<AbstractClauseCollection, Integer, AbstractClauseCollection>(){
//
//			@Override
//			public Tuple2<Integer, AbstractClauseCollection> call(AbstractClauseCollection arg0) throws Exception {
//				// TODO Auto-generated method stub
//				return new Tuple2<Integer, AbstractClauseCollection>(arg0.getKey(), arg0);
//			}
//			
//		});
//		
//		return resultingPairRDD;
//	}
	

}
