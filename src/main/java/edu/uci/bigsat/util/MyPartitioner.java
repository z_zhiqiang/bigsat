package edu.uci.bigsat.util;

import org.apache.spark.Partitioner;

public class MyPartitioner extends Partitioner{
	
	private final int number_partitions;
	
	public MyPartitioner(int num_partitions){
		this.number_partitions = num_partitions;
	}

	@Override
	public int getPartition(Object key) {
		// TODO Auto-generated method stub
		int k = (Integer) key;
		
		return partition(k, this.number_partitions);
	}

	@Override
	public int numPartitions() {
		// TODO Auto-generated method stub
		return this.number_partitions;
	}
	
	public boolean equals(Object obj){
		if(this == obj){
			return true;
		}
		
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		
		MyPartitioner c = (MyPartitioner) obj;
		
		return this.number_partitions == c.number_partitions;
	}
	
	public static int partition(int key, int number_partitions){
		return key % number_partitions;
	}

}
