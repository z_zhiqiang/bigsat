#!/bin/bash                                                                                                                                                                             
FILES_app16='/home/salamian/sanaz/Benchmarks_satsolver_2016/app16/app16/'
for f in  "$FILES_app16"*.cnf
do
   start=$(date +%s.%N);
   timeout 1800s taskset -c 0-31 ./glucose-syrup_static  $f >result_app16_32_memoryMonitoring.txt
    result=$(grep -c "UNSATISFIABLE" 'result_app16_32_memoryMonitoring.txt');
   if [ $result -gt 0 ]
   then
        printf "UNSATISFIABLE" >>finalFile_app16_32_memoryMonitoring.txt
   else
       resultt=$(grep -c "SATISFIABLE" 'result_app16_32_memoryMonitoring.txt' );

       if [ $resultt -gt 0 ]
       then
           printf "SATISFIABLE" >>finalFile_app16_32_memoryMonitoring.txt
       else
           printf "UNKNOWN" >>finalFile_app16_32_memoryMonitoring.txt
       fi
   fi

   dur=$(echo "$(date +%s.%N) - $start" | bc);
   printf "Execution time: %.6f seconds\n" $dur >>finalFile_app16_32_memoryMonitoring.txt
   printf "File name: %s \n" $f >> finalFile_app16_32_memoryMonitoring.txt
    free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }'  >>finalFile_app16_32_memoryMonitoring.txt
   df -h | awk '$NF=="/"{printf "Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}' >>finalFile_app16_32_memoryMonitoring.txt
   top -bn1 | grep load | awk '{printf "CPU Load: %.2f\n", $(NF-2)}' >>finalFile_app16_32_memoryMonitoring.txt
   printf "***************************************************\n">>finalFile_app16_32_memoryMonitoring.txt
done
