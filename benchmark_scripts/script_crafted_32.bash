#!/bin/bash                                                                                                                                                                             
FILES_crafted='/home/salamian/sanaz/Benchmarks_satsolver_2016/crafted16/crafted16/'
for ff in "$FILES_crafted"*.cnf
do
    start=$(date +%s.%N);
   timeout 1800s taskset -c 0-31 ./glucose-syrup_static  $ff >result_crafted_32_mm.txt
   result=$(grep -c "UNSATISFIABLE" 'result_crafted_32_mm.txt');
   if [ $result -gt 0 ]
   then
        printf "UNSATISFIABLE" >>finalFile_crafted_32_mm.txt
   else
       resultt=$(grep -c "SATISFIABLE" 'result_crafted_32_mm.txt' );

       if [ $resultt -gt 0 ]
       then
           printf "SATISFIABLE" >>finalFile_crafted_32_mm.txt
       else
           printf "UNKNOWN" >>finalFile_crafted_32_mm.txt
       fi
   fi
   dur=$(echo "$(date +%s.%N) - $start" | bc);
   printf "Execution time: %.6f seconds\n" $dur >>finalFile_crafted_32_mm.txt
   printf "File name: %s \n" $ff >> finalFile_crafted_32_mm.txt
 free -m | awk 'NR==2{printf "Memory Usage: %s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }'  >>finalFile_crafted_32_mm.txt
   df -h | awk '$NF=="/"{printf "Disk Usage: %d/%dGB (%s)\n", $3,$2,$5}' >>finalFile_crafted_32_mm.txt
   top -bn1 | grep load | awk '{printf "CPU Load: %.2f\n", $(NF-2)}' >>finalFile_crafted_32_mm.txt

   printf "***************************************************\n">>finalFile_crafted_32_mm.txt
done
