#!/bin/bash 

FILES_random='/home/salamian/sanaz/Benchmarks_satsolver_2016/RandomSat/RandomSat/'                                                                                                    
                                                                                                                 
for fff in  "$FILES_random"*.cnf                                                                                                                                                      
do                                                                                                                                                                                    
  start=$(date +%s.%N);                                                                                                                                                              
  timeout 1800s ./glucose-syrup_static  $fff                                                                                                                                         
  dur=$(echo "$(date +%s.%N) - $start" | bc);                                                                                                                                        
  printf "Execution time: %.6f seconds\n" $dur >>finalFile_random.txt                                                                                                                
  printf "File name: %s \n" $fff >> finalFile_random.txt                                                                                                                               
  printf "***************************************************\n">>finalFile_random.txt                                                                                                
done         